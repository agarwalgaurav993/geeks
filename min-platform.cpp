#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int end(int start, int end){
	if(start <= end)
		return end;
	return 2400 + end;
}
int minPlatform(vector<int> &a, vector<int> &d){
	vector<int> t(5005);
	for(int i=0; i<size(a); i++){
		t[a[i]]++;
		t[end(a[i], d[i])+1]--;
	}

	int mx = 1;
	int sum = 0;
	int i;
	for(int j=1; j < 5005; j++){
		t[j] += t[j-1];
		mx = max(t[j], mx);
	}

	return mx;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<int> a(N);
		vector<int> b(N);
		for(int i=0; i<N; i++)
			cin>>a[i];
		
		for(int i=0; i<N; i++)
			cin>>b[i];
		cout<<minPlatform(a, b)<<endl;
		
	}
	return 0;
}