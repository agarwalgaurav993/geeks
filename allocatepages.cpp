#include <bits/stdc++.h>
using namespace std;

int gmax = -1;
bool checkYesNo(int each, int M, vector<int> & ar){
    /*Currently This function would return True if ar[i] can be lower bound of target element*/
    int sum = 0;
    int cnt = 1;
    for(int i=0; i<ar.size(); i++){
    	if(ar[i] > each)
    		return false;
    	if(sum + ar[i] > each){
    		sum = ar[i];
    		cnt++;
    	}
    	else
    		sum += ar[i];
    }
    return cnt <= M;
}
/*B-Search to Return First Yes from the array of [N, N, N, N, Y]*/
/*Function returns index of the First Yes in case Yes is present, else returns -1*/
/*Note: Function to Select "m"*/
int BSearch(int s, int e, int target, vector<int>& ar, int result){
        if(s > e)
            return -1;
        if(s == e){
        	return min(result,s);
            /*if(checkYesNo(s, target, ar))
                return s;
            else
                return -1;*/
        }
        int m = (s + e)/2;
        //cout<<m<<endl;
        if(checkYesNo(m, target, ar)){
            e = m;
            result = min(result, m);
            //cout<<"True\n";
            return BSearch(s, e, target, ar,  result);
        }
        else{
            s = m + 1;
            //cout<<"False\n";
            return BSearch(s, e, target, ar, result);
        }
}
int main(){
	int T;
	cin>>T;
	while(T--){
		gmax = -1;
		int N, M;
		cin>>N;
		vector<int> v(N);
		int sum = 0;
		for(int i=0; i<N; i++){
			cin>>v[i];
			sum += v[i];
		}

		cin>>M;
		cout<<BSearch(0, sum, M, v, INT_MAX)<<endl;
	}
	return 0;
}