#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;
ll exp(ll a, ll b, ll d){
	int bits = floor(log2(b)) + 1;
	int num = (1<<(bits - 1));
	ll p = 1;
	while(num){
		if((num&b) != 0)
			p = (((p*p)%d)*a)%d;
		else
			p = (p*p)%d;
		num = (num>>1);
	}
	return p;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		
		int a, b, c;
		cin>>a>>b>>c;
		cout<<exp(a, b, c)<<endl;
	}
	return 0;
}