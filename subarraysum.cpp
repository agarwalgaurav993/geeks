#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;


void getPossible(vector<int> &v, int sum){
	int i=0, j = 0;
	int csum = 0;
	bool f = true;
	int s, e;
	while(j < size(v) && f){
		if(csum == sum){
			f = false;
			s = i+1;
			e = j;
		}
		else if(csum + v[j] <= sum)
			csum += v[j++];
		else if(csum + v[j] > sum)
			csum -= v[i++];
		if(i>j){
			j = i;
			sum = 0;
		}
		//cout<<csum<<endl;
	}
	if(sum == csum )
	{
		f = false;
		s = i+1;
		e = j;
	}
	if(f || sum == 0)
		cout<<-1<<endl;
	else
		cout<<s<<" "<<e<<endl;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, sum;
		cin>>N>>sum;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		getPossible(v, sum);
	}	
	return 0;
}