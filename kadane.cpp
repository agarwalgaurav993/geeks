#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int KadaneAlgorithm(vector<int> &v){
	int mx = INT_MIN;
	int csum = 0;
	for(int i=0; i<size(v); i++){
		if(csum < 0 )
			csum = 0;
		csum += v[i];
		mx = max(csum, mx);
	}
	return mx;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		cout<<KadaneAlgorithm(v)<<endl;
	}
	return 0;
}