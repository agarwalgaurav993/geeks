#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int SubsetSum(int sum, int N){
	vector<int> v;
	for(int i=1; i <= (int)pow(sum, 1.0/N); i++){
		v.pb(pow(i, N));
	}
	vector<int> t(sum+1);
	t[0] = 1;
	for(int i=0; i<size(v); i++){
		for(int j=sum; j>=v[i]; j--){
			if(t[j - v[i]] > 0)
				t[j]+= t[j - v[i]];
		}
	}
	return t[sum];
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int x, n;
		cin>>x>>n;
		cout<<SubsetSum(x, n)<<endl;
	}
	return 0;
}