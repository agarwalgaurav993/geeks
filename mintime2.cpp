#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int c(int n, vector<int> &v, int x, int y, int z){
	if(v[n] != -1)
		return v[n];
	v[n] = c(n-1, v, x, y, z) + x;
	if((n&1))
		v[n] = min(v[n], c((n+1)/2, v, x, y, z) + z + y);
	else
		v[n] = min(v[n], c(n/2, v, x, y, z) + z);
	return v[n];
}


int main(){
	int T;
		cin>>T;
		for(int f=1; f<=T; f++){
			int N, x, y, z;
			cin>>N>>x>>y>>z;
			vector<int> v(N+1, -1);
			v[0] = 0;
			v[1] = x;
			cout<<c(N, v, x, y, z)<<endl;
		}	
	return 0;
}