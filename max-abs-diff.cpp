#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int maxAbsDiff(vector<int> &v){
	vector<int> minv(size(v), INT_MAX);
	vector<int> maxv(size(v), INT_MIN);
	/*Construct Min*/
	int csum, cmin = INT_MAX;
	for(int i=0 ;i<size(v); i++){
		csum = 0;
		cmin = INT_MAX;
		for(int j=i; j<size(v); j++){
			csum += v[j];
			minv[j] = min(csum , min(minv[j], cmin));
			cmin = min(csum, cmin);
		}
	}

	/*Construct Max*/
	int cmax = INT_MIN;
	for(int i=0; i<size(v); i++){
		for(int j=i; j<size(v); j++){
			csum += v[j];
			maxv[j] = max(csum, max(maxv[j], cmax));
			cmax = max(cmax, csum);
		}
	}


	int diff = INT_MIN;
	for(int i=1; i<size(v); i++){
		diff = max(diff, abs(maxv[i] - maxv[i-1]));
		diff = max(diff, abs(maxv[i] - minv[i-1]));
		diff = max(diff, abs(minv[i] - maxv[i-1]));
		diff = max(diff, abs(minv[i] - minv[i-1]));
	}
	return diff;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t;
		cin>>N;
		vector<int> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		cout<<maxAbsDiff(arr)<<endl;
	}
	return 0;
}