#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int cString(string &a, string &b){
	vector<vector<int> > v(a.length()+1, vector<int>(b.length() +1 ));
	int m = 0;
	for(int i=1; i<size(v); i++)
		for(int j=1; j<size(v[i]); j++){
			if(a[i-1] == b[j-1])
				v[i][j] = v[i-1][j-1] +1;
			m = max(v[i][j], m);
		}
	return m;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int x, y;
		cin>>x>>y;
		string a, b;
		cin>>a>>b;
		cout<<cString(a, b)<<endl;
	}
	return 0;
}