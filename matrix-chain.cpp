#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

ll multiply(vector<vector<ll> > &v, int i, int j, vector<ll> &p){
	if(v[i][j] != LLONG_MAX)
		return v[i][j];

	for(int k=i+1; k<=j ; k++){
		v[i][j] = min(multiply(v, i, k, p) + multiply(v, k, j, p) + p[i]*p[k]*p[j], v[i][j]);
	}
	return v[i][j];
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		ll N, t;
		cin>>N;
		vector<ll> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		vector<vector<ll> > v(N, vector<ll>(N, LLONG_MAX));
		for(int i=0; i<N; i++)
			v[i][i] = 0;
		for(int i=0; i<N-1; i++)
			v[i][i+1] = 0;
		cout<<multiply(v, 0, N-1, arr)<<endl;
	}
	return 0;
}