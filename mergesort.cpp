#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
#define D 1000000007
using namespace std;

void print(vector<int> &v){
	for(int i=0; i<size(v); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}
int merge(vector<int> &v, int s, int e){
	vector<int> l;
	vector<int> r;
	int m = (s+e)/2;
	int cnt = 0;
	for(int i=s; i<=m; i++)
		l.pb(v[i]);

	for(int i=m+1; i<=e; i++)
		r.pb(v[i]);

	for(int k=s, i=0, j=0; k<=e; k++){
		if(j == size(r) || (i<size(l) && l[i] < r[j]))
			v[k] = l[i++];
		else{
			v[k] = r[j++];
			cnt = (cnt + size(l) - i)%D ;
		}
	}
	return cnt%D;
}

int mergesort(vector<int> &v, int s, int e){
	if(s>=e)
		return 0;
	int m = (s+e)/2;
	int cnt = mergesort(v, s,m);
	cnt = (cnt + mergesort(v, m+1, e))%D;
	cnt = (cnt + merge(v, s, e))%D;
	return cnt;
}


int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		
		cout<<mergesort(v, 0, size(v)-1)<<endl;
		print(v);
	}
	return 0;
}