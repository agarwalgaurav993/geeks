#include<bits/stdc++.h>
#define ll long long
using namespace std;
int myRank(vector<ll > &rank, int s, int e, ll r){
    if(s == e){
        return s;
    }
    int m = s + (e - s + 1)/2;
    if(rank[m] <= r)
        return myRank(rank, m, e, r);
    else
        return myRank(rank, s, m-1, r);
}
ll getMarks(vector<ll> &rank, vector<ll> &marks, ll r){
    int mr = myRank(rank, 0, rank.size()-1, r);

    ll diff = r - rank[mr];
    return marks[mr] + diff;
}
int main()
 {
	//code
	int T;
	cin>>T;
	while(T--){
	    int N, Q;
	    cin>>N>>Q;
	    vector<ll> marks(N);
	    vector<ll> rank(N);
	    ll r = 1;
	    for(int i=0; i<N; i++){
	        ll a, b;
	        cin>>a>>b;
	        marks[i] = a;
	        rank[i] = r;
	        //cout<<rank[i]<<" ";
	        r += b-a+1;
	    }
	    
	    for(int i=0; i<Q; i++){
	        ll q;
	        cin>>q;
	        cout<<getMarks(rank, marks, q)<<" ";
	    }
	    cout<<endl;
	}
	return 0;
}