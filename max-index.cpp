#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;


int getMax(vector<int> &v){
	int mx = 0;
	vector<vector<int> > t(101);
	for(int i=0; i<size(v); i++)
		t[v[i]].pb(i);

	for(int i=0; i<101; i++)
		if(!t[i].empty())
			for(int j=i; j<101; j++)
				if(!t[j].empty())
					mx = max(mx, t[j].back() - t[i][0]);
	return mx;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		cout<<getMax(v)<<endl;
	}
	return 0;
}