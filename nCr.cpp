#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define D 1000000007
#define mp make_pair
using namespace std;

vector<ll> nCr(int N){
	vector<ll> v(N+1);
	v[0] = 1;
	for(int i=1; i<=N-i; i++){
		v[i] = ((v[i-1]*(N-i+1))/i)%D;
		v[N-i] = v[i];
	}
	v[N] = 1;
	return v;
}
void print(vector<ll> &v){
	for(int i=0; i<size(v); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}
ll getCombinations(int N){
	vector<ll> v = nCr(N);
	ll p = 0;
	for(int i=0; i <= N; i++){
		p = (p + (v[i]*v[i])%D)%D ;
	}
	return p;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		cout<<getCombinations(N)<<endl;
	}
	return 0;
}