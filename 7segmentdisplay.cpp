#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
using namespace std;
int mp[] = {6, 2, 5, 5, 4, 5, 6, 3, 7, 5};
char rmp[] = {'q', 'q', '1', '7', '4', '2', '0', '8'};

int count(string &s){
	int c = 0;
	for(int i=0; i < s.length(); i++)
		c += mp[ s[i] - '0' ];
	return c;
}

string getMyString(int N, int C){
	C = C - N*2;
	vector<int> v(N, 2);
	int i;
	for(i=0; i<N && C-4 >= 0; i++){		
		v[i] += 4;
		C = C-4;
	}
	if(i<N && C)
		v[N-1] += C;
	else{
		for(i=N-1; i>=0 && C; i--){
			v[i] += 1;
			C--;
		}
	}
	string s;
	for(int i=0; i<N; i++){
		s.pb(rmp[v[i]]);
	}
	return s;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		string s;
		cin>>s;
		cout<<getMyString(s.length(), count(s))<<endl;	
	}
	return 0;
}