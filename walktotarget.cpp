#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int walks(vector<int> &q, vector<vector<int> > &graph, int k, int d){
	if(k == 0)
		return q[d];
	vector<int> r(size(graph));
	for(int i=0; i<size(q); i++){
		if(q[i])
			for(int j=0; j < size(graph); j++){
				if(graph[i][j])
					r[j] += q[i];
			}
	}
	return walks(r, graph, k-1, d);
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int s, d, k, N;
		cin>>N;
		vector<vector<int> > graph(N, vector<int>(N));
		for(int i=0; i<N; i++)
			for(int j=0; j<N; j++)
				cin>>graph[i][j];

		cin>>s>>d>>k;
		vector<int> q(N);
		q[s] = 1;
		cout<<walks(q, graph, k, d)<<endl;
	}
	return 0;
}