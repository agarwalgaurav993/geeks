#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;


int  getLCSMatrix(string &a, string &b){
	vector<vector<int> > v(a.length()+1, vector<int>(b.length() + 1));

	for(int i=1; i< size(v); i++)
		for(int j=1; j<size(v[i]); j++){
			if(a[i-1] == b[j-1])
				v[i][j] = 1 + v[i-1][j-1];
			else
				v[i][j] = max(v[i-1][j], v[i][j-1]);
		}
	return v[a.length()][b.length()];
}

int main(){
	
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int c, d;
		cin>>c>>d;
		string a, b;
		cin>>a>>b;
		cout<<getLCSMatrix(a, b)<<endl;
	}
	return 0;
}	