#include <bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

bool check(int curr, int target){
	return curr >= target;
}
int b_search(int s, int e, vector<int> &v, int target){
	if(s == e){
		if(check(v[s], target))
			return s;
		return -1;
	}
	int m = (s+e)/2;
	if(check(v[m], target))
		return b_search(s, m, v, target);
	else
		return b_search(m+1, e, v, target);
}
int getAr( int i, vector<int> &v){
	if(i<0)
		return INT_MIN;
	if(i>= v.size())
		return INT_MAX;
	return v[i];
}
int main(){
	int T;
	cin>>T;
	while(T--){
		int N, t;
		cin>>N;
		vector<int> ar;
		for(int i=0; i<N; i++){
			cin>>t;
			ar.push_back(t);
		}
		int K, X;
		cin>>K>>X;
		int ptr = b_search(0, ar.size()-1, ar, X);
		float m = K;
		m = ceil(m/(float)2);
		cout<<m<<endl;
		int s = max(ptr - (int)m, 0);
		for(int i=s; i<ptr; i++)
		    cout<<ar[i]<<" ";
		    
		    
		int e = min((int)ar.size()-1, ptr + K - ptr + s);
		for(int i=ptr+1; i<=e; i++)
		    cout<<ar[i]<<" ";
		cout<<endl;
	}
	return 0;
}