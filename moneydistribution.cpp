#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

bool canDistribute(vector<int> &v, int amt1){
	int amt2 = 0;
	for(int i=0; i<size(v); i++){
		amt2 += v[i];
	}
	if(amt2 == amt1)
		return true;
	if(amt2 - amt1 < 0 || ((amt2 - amt1)&1))
		return false;
	int k = (amt2 - amt1)>>1;

	vector<int> t(k+1);
	t[0] = 1;
	for(int i=0; i<size(v); i++){
		for(int j=k; j>=v[i]; j--){
			t[j] = t[j] || t[j - v[i]];
		}
	}
	return t[k];
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, M;
		cin>>N>>M;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		cout<<canDistribute(v, M)<<endl;
	}
	return 0;
}