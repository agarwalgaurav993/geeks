#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
#define M 1000000007
using namespace std;
/*ll centWays(int amt, vector<int> &v){
	if(amt <= 2)
		return amt;
	if(v[amt] != -1)
		return v[amt];
	v[amt] = (centWays(amt - 1, v) + centWays(amt - 2, v))%M;
	return v[amt];
}*/
ll centWays(vector<int> &c, int amt){
	vector<ll> v(amt+1);
	v[0] = 1;
	for(int i=0; i<size(c); i++){
		for(int j=c[i]; j<=amt; j++){
			if(v[j - c[i]] != 0){
				v[j] += v[j - c[i]];
			}
		}
	}
	return v[amt];
}
int main(){
	vector<int> c;
	c.pb(1);
	c.pb(2);
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int amt;
		cin>>amt;
		cout<<centWays(c, amt)<<endl;
	}	
	return 0;
}