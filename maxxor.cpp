#include<bits/stdc++.h>
using namespace std;
int main()
 {
	//code
	int T;
	cin>>T;
	while(T--){
	    int N;
	    cin>>N;
	    long long c = 0;
	    int pos = 0;
	    while(N){
	        if((N&1) == 0)
	           c += (long long)(1<<pos);
	        N = (N>>1);
	        pos++;
	    }
	    cout<<c<<endl;
	}
	return 0;
}