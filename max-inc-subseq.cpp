#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;
void print(vector<int> &v){
	for(int i=0; i<size(v); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}
int longIncSub(vector<int> &v){
	vector<int> t(size(v));
	t[0] = 1;
	for(int i=1; i<size(v); i++){
		for(int j=0; j<i; j++){
			if(v[j] < v[i])
				t[i] = max(t[i],1+ t[j]);
 		}
	}
	int m = 0;
	//print(t);
	for(int i=0; i<size(t); i++)
		m = max(m, t[i]);
	return m;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t;
		cin>>N;
		vector<int> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		cout<<longIncSub(arr)<<endl;
	}
	return 0;
}