#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

ll ways(int N){
	if(N < 2)
		return 1;
	vector<ll> v(N+1);
	v[0] = 1;
	v[1] = 1;
	for(int i=2; i<=N; i++){
		v[i] = v[i-1] + v[i-2];
		//cout<<v[i]<<" ";
	}
	return v[N];
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t;
		cin>>N;
		cout<<ways(N)<<endl;
	}
	return 0;
}