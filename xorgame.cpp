#include<bits/stdc++.h>
using namespace std;
int main()
 {
	//code
	int T;
	cin>>T;
	while(T--){
	    int N;
	    cin>>N;
	    int cnt = 0;
	    bool flag = true;
	    while(N){
	        if((N&1) == 0){
	            flag = false;
	            break;
	        }
	        cnt++;
	        N = (N>>1);
	    }
	    if(flag == false || cnt == 0)
	        cout<<"-1"<<endl;
    	else{
    	    cnt--;
    	    cnt = (1<<cnt);
    	    if(cnt == 1)
    	        cout<<cnt;
    	    else
    	        cout<<cnt-1<<endl;
    	}
	}
	return 0;
}