#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int maxAns(vector<int> &v){
	sort(v.begin(), v.end());
	int sum = 0;
	for(int i=0; i<size(v) && v[i] <= 0; i+=2){
		sum = (sum + (v[i] * v[i+1])%D)%D;
	}
}

int main(){
	
	return 0;
}