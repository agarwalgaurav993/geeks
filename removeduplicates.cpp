#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

bool same(string &s, int i, char c){
	if(i < 0 || i >= s.length())
		return false;
	return s[i] == c;
}
void rmDuplicates(string &s, string &r, int i, int j){
	if(i>j)
		return;
	
	if(i<j){
		if(!same(s, i-1, s[i]) && !same(s, i+1, s[i]))
			r.pb(s[i]);
		rmDuplicates(s, r, i+1, j-1);

		if(!same(s, j-1, s[j]) && !same(s, j+1, s[j]))
			r.pb(s[j]);
		return;
	}
	
	if(i == j){
		if(!same(s, i-1, s[i]) && !same(s, i+1, s[i]))
			r.pb(s[i]);
	}
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		string s;
		cin>>s;
		string r;
		int p = -1;
		while(s.length() != p){
			p = s.length();
			rmDuplicates(s, r, 0, s.length()-1);
			s = r;
			r = "";
		}
		cout<<s<<endl;
	}
	return 0;
}