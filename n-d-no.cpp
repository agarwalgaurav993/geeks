#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;
void print(vector<int> &v){
	for(int i=0; i<size(v); i++)
		cout<<v[i];
	cout<<" ";
}

void recur(vector<int> &v, int n, int l){
	if(n == 0)
		print(v);

	for(int i=l+1; i<=9; i++){
		v.pb(i);
		recur(v, n-1, i);
		v.pop_back();
	}
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<int> v;
		recur(v, N, 0);
		cout<<endl;
	}
	return 0;
}