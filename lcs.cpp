#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;


set<string> t;

bool getStrings(vector<vector<int> > &v, string &s, int i, int j, string &a){
	if(i == 0 || j==0){
		reverse(s.begin(), s.end());
		t.insert(s);
		reverse(s.begin(), s.end());
		return true;
	}
	if(v[i][j] == v[i-1][j-1] + 1){
		s.pb(a[i-1]);
		getStrings(v, s, i-1, j-1, a);
		s.pop_back();
	}
	else{
		if(v[i-1][j] == v[i][j])
			getStrings(v, s, i-1, j,  a);
		if(v[i][j-1] == v[i][j])
			getStrings(v, s, i, j-1, a);
	}
}

vector<vector<int> >  getLCSMatrix(string &a, string &b){
	vector<vector<int> > v(a.length()+1, vector<int>(b.length() + 1));

	for(int i=1; i< size(v); i++)
		for(int j=1; j<size(v[i]); j++){
			if(a[i-1] == b[j-1])
				v[i][j] = 1 + v[i-1][j-1];
			else
				v[i][j] = max(v[i-1][j], v[i][j-1]);
		}
	return v;
}

int main(){
	
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		string a, b;
		cin>>a>>b;
		vector<vector<int> > v = getLCSMatrix(a, b);
		t.clear();
		string s;
		getStrings(v, s, a.length(), b.length(), a);
		set<string> :: iterator it ;
		for(it = t.begin(); it != t.end(); it++)
			cout<<*it<<" ";
		cout<<endl;
	}
	return 0;
}	