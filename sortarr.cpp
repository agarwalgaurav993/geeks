#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;


class Compare
{
public:
    bool operator() (pair<int, int> &a, pair<int, int> &b)
    {
        if(a.first != b.first)
			return a.first < b.first;
		return a.second > b.second;
    }
};

void sortme(vector<int> &v){

	priority_queue<pair<int, int> , vector<pair<int, int> > , Compare > pq;
	vector<int> t(80);
	for(int i=0; i<size(v); i++)
		t[v[i]]++;

	for(int i=0; i<size(t); i++)
		if(t[i] != 0)
			pq.push(mp(t[i], i));

	while(!pq.empty()){
		pair<int, int> p = pq.top();
		while(p.first){
			cout<<p.second<<" ";
			p.first--;
		}
		pq.pop();
	}
	cout<<endl;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		sortme(v);
	}
	return 0;
}