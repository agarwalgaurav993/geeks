#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
#define M 1000000007
using namespace std;

int numOfPalindrome(int K, int N){
	if((N&1))
		N = N/2 + 1;
	else
		N = N/2;
	
	ll t  = 0;
	for(int i=1; i<=N; i++){
		ll p = 1;
		for(int j=K; j > K-i; j--)
			p = (p*j)%M;
		//cout<<p<<endl;
		t += p;
	}
	return t;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, K;
		cin>>K>>N;
		cout<<numOfPalindrome(K, N)<<endl;
	}
	return 0;
}