#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;
vector<vector<int> > t;

void recur(vector<int> &v, int i, vector<int> &r){
	if(i == size(v))
	{
		t.pb(r);
		return;
	}
	recur(v, i+1, r);
	r.pb(v[i]);
	recur(v, i+1, r);
	r.pop_back();
}

void print(vector<int> &v){
	cout<<"(";
	for(int i=0; i<size(v)-1; i++)
		cout<<v[i]<<" ";
	if(!v.empty())
		cout<<v.back();
	cout<<")";
}

void printer(vector<vector<int> > &v){
	for(int i=0; i<size(v)-1; i++){
		print(v[i]);
	}
	cout<<endl;
}

bool cmp(vector<int> &a, vector<int> &b){
	int i=0;
	while(i<size(a) && i<size(b)){
		if(a[i] > b[i])
			return false;
		i++;
	}

	return size(a) <= size(b);
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		sort(v.begin(), v.end());
		vector<int> r;
		recur(v, 0, r);
		sort(t.begin(), t.end(), cmp);
		printer(t);
	}
	return 0;
}