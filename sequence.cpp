#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int Ways(int max, int pos){
	if( pos <= 1 )
		return max;
	if( max < 1 || floor(log2(max)) + 1 < pos )
		return 0;

	int ways = 0;
	for(int i=max; i>=(1<<(pos-1)); i--){
		ways += Ways(i/2, pos -1);
	}
	return ways;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, M;
		cin>>N>>M;
		cout<<Ways(N, M)<<endl;
	}
	return 0;
}