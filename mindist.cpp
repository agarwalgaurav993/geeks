#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

typedef pair<int, int> iPair;

ll dfs(int s, vector<vector<ll> > &graph, vector<bool> &v){

	v[s] = 1;
	ll length = 0;
	for(int i=0; i<size(graph); i++){
		if(!v[i] && graph[s][i]> 0)
			length = max(length, graph[s][i] + dfs(i, graph, v));
	}
	v[s] = 0;
	return length;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, M, K;
		cin>>N>>M>>K;
		vector<vector<ll> > graph(N, vector<ll>(N, 0));
		for(int i=0,x,y; i<M; i++){
			cin>>x>>y;
			cin>>graph[x][y];
			graph[y][x] = max(graph[y][x], graph[x][y]);
			graph[x][y] = graph[y][x];
		}
		vector<bool> v(N, false);
		if(dfs(0, graph, v) >= K)
			cout<<1<<endl;
		else
			cout<<0<<endl;

	}
	return 0;
}