#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

vector<vector<int> > t;
void print(vector<int> &v){
	cout<<"(";
	for(int i=0; i<size(v)-1; i++)
		cout<<v[i]<<" ";
	cout<<v.back();
	cout<<")";
}
void recur(vector<int> &v, int pos, vector<int> r, int csum){
	if(csum == 0){
		t.pb(r);
		return;
	}
	if(pos == size(v) || csum < 0)
		return;
	int cnt = 0;
	while(csum - v[pos] >= 0){
		r.pb(v[pos]);
		csum -= v[pos];
		cnt++;
	}
	while(cnt){
		recur(v, pos+1, r, csum);
		csum += v[pos];
		r.pop_back();
		cnt--;
	}
	recur(v, pos+1, r, csum);

}

vector<int> format(vector<int> &v){
	sort(v.begin(), v.end());
	vector<int> t;
	int p = -1;
	for(int i=0; i<size(v); i++){
		if(p != v[i])
			t.pb(v[i]);
		p = v[i];
	}
	return t;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		t.clear();
		int N;
		cin>>N;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		int S;
		cin>>S;
		v = format(v);
		vector<int> r;
		recur(v, 0, r, S);
		if(!t.empty()){
			for(int i=0; i<size(t); i++){
				print(t[i]);
			}
		}
		else
			cout<<"Empty";
		cout<<endl;
	}
	return 0;
}