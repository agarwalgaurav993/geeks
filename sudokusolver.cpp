#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

vector<int> mymap(vector<vector<int> > &v, int i, int j){
	vector<int> r(10);
	for(int c =0; c<9; c++)
		r[v[i][c]] = 1;
	for(int c = 0; c<9; c++)
		r[v[c][j]] = 1;

	for(int c=3*(i/3); c < 3*(i/3) + 3; c++){
		for(int d=3*(j/3); d < 3*(j/3) + 3; d++)
			r[v[c][d]] = 1;
	}
	return r;
}
void next(int i, int j, int &m, int &n){
	if(j < 8){
		m=i;
		n=j+1;
	}
	else{
		m = i+1;
		n = 0;
	}
}
void print(vector<int> &v){
	for(int i=0; i<size(v); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}
bool solver(vector<vector<int> > &v, int i, int j){
	//cout<<i<<" "<<j<<endl;
	if(i == 9 || j == 9)
		return true;
	if(v[i][j] == 0){
		vector<int> mp = mymap(v, i, j);
		for(int k=1; k<10; k++){
			if(mp[k] == 0){
				v[i][j] = k;
				int m, n;
				next(i, j, m, n);
				if(solver(v, m , n))
					return true;
				v[i][j] = 0;
			}
		}
		return false;
	}
	else{
		int m, n;
		next(i, j, m, n);
		return solver(v, m, n);
	}
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		vector<vector<int> > v(9, vector<int>(9));
		for(int i=0; i<9; i++)
			for(int j=0; j<9; j++)
				cin>>v[i][j];
		
		solver(v, 0, 0);
		for(int i=0; i<9; i++){
			for(int j=0; j<9; j++)
				cout<<v[i][j]<<" ";
		}
		cout<<endl;
	}
	return 0;
}