#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;


void marker(vector<vector<char> > &v, int i, int j){
	if(i<0 || j<0 || i>= size(v) || j >= size(v[i]) || v[i][j] == 'X' || v[i][j] == 'N')
		return;
	v[i][j] = 'N';
	marker(v, i-1, j);
	marker(v, i+1, j);
	marker(v, i, j-1);
	marker(v, i, j+1);

}

void controller(vector<vector<char> > &v, int M, int N){

	for(int i=0; i<M; i++){
		int j = 0;
		marker(v, i, j);
		j = N -1;
		marker(v, i, j);
	}
	
	for(int j=0; j<N; j++){
		int i=0;
		marker(v, i, j);
		i = M-1;
		marker(v, i, j);
	}

	for(int i=0; i<M; i++)
		for(int j=0; j<N; j++)
			if(v[i][j] == 'N')
				v[i][j] = 'O';
			else
				v[i][j] = 'X';
}

void print(vector<vector<char> > &v, int M, int N){
	for(int i=0; i<M; i++){
		for(int j=0; j<N; j++)
			cout<<v[i][j]<<" ";
	}
	cout<<endl;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int M, N;
		cin>>M>>N;
		vector<vector<char> > v(M, vector<char>(N));
		for(int i=0; i<M; i++)
			for(int j=0; j<N; j++){
				cin>>v[i][j];
			}
		
		//print(v, M, N);
		controller(v, M, N);
		for(int i=0; i<M; i++){
			for(int j=0; j<N; j++)
				cout<<v[i][j]<<" ";
		}
		cout<<endl;
	}
	return 0;
}