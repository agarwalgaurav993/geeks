#include <bits/stdc++.h>
#define size(s) (int)s.size()
using namespace std;

int main(){
	int T;
	cin>>T;
	while(T--){
		int M, N;
		cin>>M>>N;
		int ar[M][N];
		for(int i=0; i<M; i++)
			for(int j=0; j<N; j++)
				cin>>ar[i][j];
		bool f = false;
		for(int j=0; j<N && !f; j++)
			for(int i=0; i<M && !f; i++)
				if(ar[i][j])
				{
					cout<<i<<endl;
					f = true;
				}
	}
	return 0;
}