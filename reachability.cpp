#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;


int minsteps(queue<int> &q, int s, int t){

	int n = size(q);
	for(int i=0; i<n; i++){
		int j = q.front();
		q.pop();
		if(j == t)
			return s-1;
		q.push(j+s);
		q.push(j-s);
	}
	return minsteps(q, s+1, t);
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int D;
		cin>>D;
		queue<int> q;
		q.push(0);
		cout<<minsteps(q, 1, D)<<endl;
	}
	return 0;
}