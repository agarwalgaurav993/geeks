#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int mincuts(int x, int y){
	if(y < x)
		return mincuts(y, x);
	if(x == 0 || y == 0)
		return 0;
	return y/x + mincuts(x, y - (y/x)*x);
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int x, y;
		cin>>x>>y;
		cout<<mincuts(x, y)<<endl;
	}
	return 0;
}