#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int bitdiffs(vector<int> &v){
	int tc = 0;
	for(int i=0; i<32; i++){
		int zc = 0, oc = 0;
		for(int j=0; j<size(v); j++){
			if((v[j]&1))
				oc++;
			else
				zc++;
			v[j] = (v[j]>>1);
		}
		cout<<zc<<" "<<oc<<endl;
		tc += zc*oc;
	}
	return tc;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		cout<<bitdiffs(v)*2<<endl;
	}
	return 0;
}