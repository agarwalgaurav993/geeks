#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
#define MAX 50000
#define D 1000000007
using namespace std;

vector<int> sieve(){
	vector<int> v(MAX);
	vector<int > p;
	for(int i=2; i<MAX; i++){
		if(v[i] != 0)
			continue;
		p.pb(v[i]);
		for(int j = i+i; j<MAX; j++)
			v[j] = 1;
	}
	return p;
}

int segmented(int s, int e, vector<int> &p){
	vector<int> v(e -s+1);
	for(int i=0; i<size(p); i++){
		int f = s/p[i];
		for(; f <= e/p[i]; f++){
			if(p[i]*f >=s && p[i]*f <=e)
				v[p[i]*f - s] = 1;
		}
	}
	ll p = 1;
	for(int i=0; i<size(v); i++){
		if(v[i] == 0){
			p = (p*(s+i))%D;
		}
	}
	return p;
}


int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int l, r;
		cin>>l>>r;
	}
	return 0;
}