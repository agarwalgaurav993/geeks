#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

bool cmp(int n1, int n2){
	int t1=n1, c1=0;
	int t2=n2, c2=0;
	while(n1){
		c1++;
		n1 = n1/10;
	}
	while(n2){
		c2++;
		n2 = n2/10;
	}
	return n1*pow(10,c2)+n2 >= n2*pow(10,c1)+n2;
	/*vector<int> v1;
	while(n1){
		v1.pb(n1%10);
		n1 = n1/10;
	}
	
	vector<int> v2;
	while(n2){
		v2.pb(n2%10);
		n2 = n2/10;
	}
	
	int l = 0;
	while(!v1.empty() && !v2.empty() && v1.back() == v2.back()){
		l = v1.back();
		v1.pop_back();
		v2.pop_back();
	}

	if(!v1.empty() && !v2.empty())
		return v1.back() > v2.back();

	if(v1.empty() || v2.empty()){
		if(v1.empty() && v2.empty())
			return true;
		if(v1.empty())
			return l >= v2.back();
		return v1.back() >= l;
	}*/
}
void print(vector<int> &v){
	for(int i=0; i<size(v); i++)
		cout<<v[i];
	cout<<endl;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<int> v(N);
		for(int i=0; i<N; i++){
			cin>>v[i];
			assert(0<=v[i] && v[i] <=1000);
		}
		sort(v.begin(), v.end(), cmp);
		print(v);
	}
	return 0;
}