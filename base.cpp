#include<bits/stdc++.h>
using namespace std;
int main()
 {
	//code
	int T;
	cin>>T;
	while(T--){
	    int n, d;
	    cin>>n>>d;
	    bool flag = false;
	    for(int i=2; i<=32; i++){
	    	//cout<<"Base "<<i<<" : "<<floor(log(n)/ log(i)) + 1<<endl;
	       if(d == floor(log(n)/ log(i)) + 1){
	            flag = true;
	            break;
	       }
	    }
	    if(flag)
	        cout<<"Yes"<<endl;
	    else
	        cout<<"No"<<endl;
	}
	return 0;
}