#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;
int gm = INT_MAX;
int TravellingSalesman(vector<vector<int> > &v, int s, vector<int> &visited){
	bool f = true;
	int m = INT_MAX;
	visited[s] = 1;
	for(int i=0; i<size(v); i++){
		int c = 0;
		if(!visited[i]){
			f = false;
			c = v[s][i] + TravellingSalesman(v, i, visited);
			m = min(c, m);
		}
	}
	visited[s] = 0;
	if(!f)
		return m;
	else
		return v[s][0];

}


int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<vector<int> > v(N, vector<int>(N));
		for(int i=0; i<N; i++)
			for(int j=0; j<N; j++)
				cin>>v[i][j];
		vector<int> visited(N);
		cout<<TravellingSalesman(v, 0, visited)<<endl;
	}
	return 0;
}