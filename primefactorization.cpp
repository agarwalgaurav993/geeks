#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
#define MAX 1000000
using namespace std;

void seive(int start, int end, vector<int> &primes){
	vector<int> v(MAX, 1);
	for(int i = 0; i < size(primes); i++)
		for(int j = start/primes[i]; j <= end/primes[i]; j++)
			if( primes[i]*j >= start)
				v[ (primes[i]*j) - start ] = 0;
	for(int i = start; i < end; i++){
		if(v[ i - start ] && i > 1){
			primes.push_back(i);
			for(int p = i; p <= end/i; p++){
				v[ (i*p)- start ] = 0;
			}
		}
	}
}

/*
Input: range - It should be in multiple of 10^5
Output : prime numbers
*/
vector<int> segmented_seive(int range){
	vector<int> primes;
	int s = 0, e = MAX;
	while(e <= range){
		seive(s, e, primes);
		//cout<<primes.size()<<endl;
		s = e;
		e += MAX;
	}
	return primes;
}


/*int primefactors(ll s, ll e, vector<int> &p){
	vector<int> v(e-s+1);
	for(int i=0; i<size(v); i++)
		v[i] = s+i;

	int cnt = 0;
	for(int i = 0; i < size(p); i++){
		ll j = floor((double)(s-1)/p[i])+1;
		j =  p[i]*j;
		for( ; j <= e; j+=p[i]){
			int k = j - s;
			while(v[k]%p[i] == 0){
				cnt++;
				v[k] = v[k]/p[i];
			}
		}
	}
	for(int i=0; i<size(v); i++)
		if(v[i] > 1)
			cnt++;
	return cnt;
}
*/
int primefactors(ll s, ll e, vector<int> &p){
	ll cnt = 0;
	s = s-1;
	ll pcnt = 1;
	for(int i=0; i< size(p) && p[i] < s; i++){
		ll t = p[i];
		while(t>0 && t <= e && e/t - s/t){
			cnt += (e/t - s/t);
			t = t*p[i];
		}
		//cout<<p[i]<<" "<<cnt<<endl;
	}
	s++;
	vector<int> v(e-s+1, 1);
	for(int i=0; i<size(v); i++)
		v[i] = s+i;

	for(int i = 0; i < size(p); i++){
		ll j = floor((double)(s-1)/p[i])+1;
		j =  p[i]*j;
		for( ; j <= e; j+=p[i]){
			int k = j - s;
			while(v[k]%p[i] == 0){
				v[k] = v[k]/p[i];
			}
		}
	}
	for(int i=0; i<size(v); i++)
		if(v[i] > 1)
			cnt++;
	return cnt;
}

int main(){
	vector<int> p = segmented_seive(MAX);
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		ll N, A, B;
		cin>>N>>A>>B;
		//cout<<A<<B<<endl;
		cout<<primefactors(A, B, p)<<endl;
	}
	return 0;
}