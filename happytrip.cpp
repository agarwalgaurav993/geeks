#include<bits/stdc++.h>
using namespace std;
vector<pair<int, int> > v(3);
void getMin(vector<int> &a, vector<int> &b, vector<int> &c){
    v[0].first = 0;
    v[1].first = 0;
    v[2].first = INT_MAX;
    sort(a.begin(), a.end());
    sort(b.begin(), b.end());
    sort(c.begin(), c.end());
    vector<pair<int, int> > t(3);
    int i = 0, j = 0, k = 0;
    t[0] = make_pair(a[i++],1);
    t[1] = make_pair(b[j++],2);
    t[2] = make_pair(c[k++], 3);
    bool f = false;
    int m = INT_MAX;
    while(!f){
        sort(t.begin(), t.end());
        //cout<<t[0].first<<" "<<t[1].first<<" "<<t[2].first<<endl;

        if(m > abs(t[2].first - t[0].first) || (m == abs(t[2].first - t[0].first) && t[0].first + t[1].first + t[2].first < v[0].first + v[1].first + v[2].first)){
            m = abs(t[2].first - t[0].first);
            v = t;
        }
        pair<int, int> p = t[0];
        if(p.second == 1 ){
            if(i < a.size())
                p.first = a[i++];
            else
                f = true;
        }
        else if(p.second == 2){
            if(j < b.size())
                p.first = b[j++];
            else
                f = true;
        }
        else if(p.second == 3){
            if(k < c.size())
                p.first = c[k++];
            else
                f = true;
        }
        t[0] = p;
    }
    
}
int main()
 {
	//code
	int T;
	cin>>T;
	while(T--){
	    int N;
	    cin>>N;
	    vector<int> a(N);
	    vector<int> b(N);
	    vector<int> c(N);
	    for(int i=0; i<N; i++){
	        cin>>a[i];
	    }
	    for(int i=0; i<N; i++){
	        cin>>b[i];
	    }
	    for(int i=0; i<N; i++){
	        cin>>c[i];
	    }
	    getMin(a, b, c);
	    cout<<v[0].first<<" "<<v[1].first<<" "<<v[2].first<<endl;
	}
	return 0;
}