#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int getMinHeight(vector<int> &v, int k){
	if(size(v) < 2)
		return 0;
	sort(v.begin(), v.end());
	int m = v[size(v) - 1] - v[0];
	int lower = v[0] + k;
	int upper = v[size(v) - 1] -k;
	if(lower > upper)
		swap(lower, upper);

	for(int i=1; i<size(v)-1; i++){
		int inc = v[i] + k;
		int dec = v[i] - k;
		if(inc <= upper || dec >= lower)
			continue;

		if(abs(upper - dec) <= abs(inc - lower))
			lower = dec;
		else
			upper = inc;
	}
	return min(m, abs(upper - lower));
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int K, N;
		cin>>K>>N;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		cout<<getMinHeight(v, K)<<endl;
	}
	return 0;
}