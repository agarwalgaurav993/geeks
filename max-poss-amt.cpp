#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

void print(vector<vector<int> > &v){
	for(int i=0; i<size(v); i++){
		for(int j=0; j<size(v[i]); j++)	
			cout<<v[i][j]<<" ";
		cout<<endl;
	}
	
}
int maxAmount(vector<int> &a){
	vector<vector<int> > r(size(a), vector<int> (size(a)));
	/*Range Sum*/
	for(int i=0; i<size(r); i++){
		int sum = 0;
		for(int j=i; j<size(r[i]); j++){
			sum += a[j];
			r[i][j] = sum;
		}
	}
	/*Game*/
	vector<vector<int> > v(size(a), vector<int> (size(a)));

	for(int i=0; i<size(v); i++)
		v[i][i] = a[i];

	for(int s=1; s<size(v); s++){
		for(int i=0; i < size(v)-s; i++){
			int j = i + s;
			v[i][j] = max(a[i] + r[i+1][j] - v[i+1][j], r[i][j-1] - v[i][j-1] + a[j]);
		}
	}
	return v[0][size(a)-1];
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t;
		cin>>N;
		vector<int> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		cout<<maxAmount(arr)<<endl;
	}
	return 0;
}