#include <bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
#define M 1000000007
using namespace std;
ll getValue(ll n, unordered_map<ll, ll>& mp){
	if(mp.find(n) != mp.end())
		return mp[n];
	else if((n&1) == 0){
		mp[n] = ((getValue(n/2, mp)*((2*getValue(n/2 + 1, mp))%M - (getValue(n/2, mp)))%M) + M)%M;
	}
	else if((n&1) == 1){
		mp[n] = (((getValue(n/2, mp)*getValue(n/2, mp)))%M + ((getValue(n/2 +1, mp)*getValue(n/2 +1, mp)))%M)%M;
	}
	return mp[n];
}
int main(){
	int T;
	cin>>T;
	while(T--){
		unordered_map<ll, ll> mp;
		mp[0] = 0;
		mp[1] = 1;
		mp[2] = 1;
		mp[3] = 2;
		ll n;
		cin>>n;
		cout<<getValue(n+1, mp)<<endl;
	}	
	return 0;
}