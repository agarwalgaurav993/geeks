#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

void print(vector<vector<int> > &v){
	for(int i=0; i<size(v); i++){
		for(int j=0; j<size(v[i]); j++)
			cout<<v[i][j]<<" ";
		cout<<endl;
	}
}
ll apCount(vector<int> &ar){
	vector<vector<int> > v(201, vector<int> (size(ar)));
	for(int i=0; i<size(ar); i++){
		for(int j=0; j<i; j++){
			int d = ar[j] - ar[i] + 100;
			//cout<<d<<endl;
			if(d == 100)
				continue;
			v[d][i] = v[d][i] + v[d][j] + 1;
		}
	}
	//print(v);
	ll sum = 0;
	for(int i=0; i<size(v); i++){
		for(int j=0; j<size(v[i]); j++)
			sum += v[i][j];
	}
	//cout<<sum<<endl;
	vector<int> t(101);
	for(int i=0; i<size(ar); i++){
		t[ar[i]]++;
	}
	for(int i=0; i<size(t); i++){
		sum += (pow(2, t[i]) - 1);
	}
	return sum+1;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t;
		cin>>N;
		vector<int> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		cout<<apCount(arr)<<endl;
	}
	return 0;
}