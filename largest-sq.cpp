#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int largestSq(vector<vector<int> > &v){
	int m = 0;
	for(int i=0; i<size(v); i++){
		for(int j=0; j<size(v[i]); j++){			
			if(i != 0 && j != 0 && v[i][j] == 1)
				v[i][j] = min(v[i-1][j], min(v[i-1][j-1], v[i][j-1])) + 1;
			m = max(m, v[i][j]);
		}
	}
	return m;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int M, N;
		cin>>M>>N;
		vector<vector<int> > v(M, vector<int>(N));
		for(int i=0; i<M; i++)
			for(int j=0; j<N; j++)
				cin>>v[i][j];
		cout<<largestSq(v)<<endl;
	}
	return 0;
}