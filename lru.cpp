#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int pagefaults(vector<int> &v, int M){
	queue<int> q;
	int faults = 0;
	vector<bool> flag(1001, false);
	for(int i=0; i<size(v); i++){
		if(!flag[v[i]]){
			if(size(q) == M){
				flag[q.front()] = false;
				q.pop();
			}
			q.push(v[i]);
			flag[v[i]] = true;
			faults++;
		}
	}
	return faults;
}
int main(){
	int T;
	cin>>T;
	for(int c=1; c<=T; c++){
		int N, M;
		cin>>N;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		cin>>M;
		cout<<pagefaults(v, M)<<endl;
	}
	return 0;
}