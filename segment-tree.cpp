#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

/*Segment Tree to return maximum in given range*/
int createSegmentTree(vector<int> &v, int s, int e, vector<int> &st, int i){
	if(i >= size(st))
		return INT_MIN;
	if(st[i] != INT_MIN)
		return st[i];
	if(s == e)
		st[i] = v[s];
	else
		st[i] = max(createSegmentTree(v, s, (s+e)/2, st, 2*i+1), createSegmentTree(v, (s+e)/2+1, e, st, 2*i+2));
	
	return st[i];

}

/*Returns Maximum Element Segment Tree*/
vector<int> SegmentTree(vector<int> &v){
	int N = floor(log2(size(v))) + 1;
	N = (1<<N) - 1;
	vector<int> st(N, INT_MIN);
	createSegmentTree(v, 0, size(v)-1, st, 0);
	return st;
}


/*
Input : 
		Given Range (s, e)
		i - current node of segment tree
		Range of current node - (ms, me) (in terms of index values of Array v)

Output :
		Maximum in given range
*/
int getMaximum(int s, int e, int i, int ms, int me, vector<int> &v){
	//cout<<i<<" : "<<ms<<" "<<me<<endl;
	if(s<=ms && e>=me)
		return v[i];
	else if((s < ms && e < ms) || (s > me && e > me))
		return INT_MIN;
	else
		return max(getMaximum(s, e, 2*i + 1, ms, (ms+me)/2, v), getMaximum(s, e, 2*i +2, (ms+me)/2+1, me, v));
}
void print(vector<int> &v){
	for(int i=0; i < size(v); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t, Q;
		cin>>N>>Q;
		vector<int> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		vector<int> st = SegmentTree(arr);
		print(st);
		for(int i=0; i<Q; i++){
			int x, y;
			cin>>x>>y;
			cout<<getMaximum(x, y, 0, 0, size(arr)-1, st)<<endl;
		}
		
		
	}
	return 0;
}