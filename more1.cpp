#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

void print(vector<int> &v){
	for(int i=0; i<size(v); i++)
		cout<<v[i];
	cout<<" ";
}
void recur(int cnt, int pos, vector<int> &v){
	if(cnt<0)
		return;
	if(pos == 0){
		print(v);
		return;
	}
	v.pb(1);
	recur(cnt + 1, pos-1, v);
	v.pop_back();
	v.pb(0);
	recur(cnt-1, pos-1, v);
	v.pop_back();
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<int> v;
		recur(0, N, v);
		cout<<endl;
	}
	return 0;
}