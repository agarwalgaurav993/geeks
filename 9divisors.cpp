#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
#define MAX 1000000
using namespace std;

	void seive(int start, int end, vector<int> &primes){
		vector<int> v(MAX, 1);
		for(int i = 0; i < size(primes); i++)
			for(int j = start/primes[i]; j <= end/primes[i]; j++)
				if( primes[i]*j >= start)
					v[ (primes[i]*j) - start ] = 0;
		for(int i = start; i < end; i++){
			if(v[ i - start ] && i > 1){
				primes.push_back(i);
				for(int p = i; p <= end/i; p++){
					v[ (i*p)- start ] = 0;
				}
			}
		}
	}

	/*
	Input: range - It should be in multiple of 10^5
	Output : prime numbers
	*/
	vector<int> segmented_seive(int range){
		vector<int> primes;
		int s = 0, e = MAX;
		while(e <= range){
			seive(s, e, primes);
			//cout<<primes.size()<<endl;
			s = e;
			e += MAX;
		}
		return primes;
	}


int lessThanme(ll n, vector<int> &primes){
	int cnt = 0;
	for(int i=0, j = size(primes)-1; i<j;){
		ll t = primes[i];
		t = t*t;
		ll tt = primes[j];
		tt = tt*tt;
		if(tt*t < tt || tt*t > n)
			j--;
		else{
			cnt += j - i;
			i++;
		}
	}
	return cnt;
}

int lessthan8power(ll n, vector<int> &p){
	for(int i=0; i<size(p); i++){
		ll t = p[i];
		t = t*t;
		t = t*t;
		t = t*t;
		if(t < p[i] || t >= n)
			return i;
	}
	return -1;
}

int main(){
	int T;
	cin>>T;
	vector<int> p = segmented_seive(MAX);
	for(int f=1; f<=T; f++){
		ll N;
		cin>>N;
		int c1 = lessThanme(N-1, p);
		int c2 = lessthan8power(N, p);
		cout<<c1+c2<<endl;
	}	
	return 0;
}