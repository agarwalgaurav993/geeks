#include<bits/stdc++.h>
using namespace std;
bool disteq(int a1, int a2, int b1, int b2, int c1, int c2){
    int d1 = (b1 - a1)*(b1 - a1) + (b2 - a2)*(b2 - a2);
    int d2 = (c1 - b1)*(c1 - b1) + (c2 - b2)*(c2 - b2);
    return  d1 == d2;
}
bool sameline(double a1, double a2, double b1, double b2, double c1, double c2){
    if(a1 == b1  && b1 == c1)
        return true;
    if(a2 == b2 && b2 == c2)
        return true;
    
    double m1 = abs((a2 - b2)/(a1 - b1));
    double m2 = abs((b2 - c2)/(b1 - c1));
    return m1 == m2;
}
int main()
 {
	//code
	int T;
	cin>>T;
	while(T--){
	    int a1, a2, b1, b2, c1, c2;
	    cin>>a1>>a2>>b1>>b2>>c1>>c2;
	    cout<<disteq(a1, a2, b1, b2, c1, c2)<<endl;
	    cout<<sameline(a1, a2, b1, b2, c1, c2)<<endl;
/*	    if(!disteq(a1, a2, b1, b2, c1, c2) || sameline(a1, a2, b1, b2, c1, c2))
	        cout<<0<<endl;
	    else
	        cout<<"1"<<endl;*/
	}
	return 0;
}