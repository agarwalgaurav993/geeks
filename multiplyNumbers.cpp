#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

void print(vector<int> &v){
	for(int i=0; i<size(v); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}

/*
Note: LSB at 0, MSB at v.size()-1
*/
vector<int> Base2to10(vector<int> &v, int b){
	/*Note : Number in base 2 is from Right to Left i.e, LSB at 0*/
	while(size(v) > 1 && v.back() == 0)
		v.pop_back();

	int s = ceil((float)size(v)*log(b));
	vector<int> r(s);
	int c =0;
	while(!v.empty()){
		c = v.back();
		for(int i=0; i<s; i++){
			r[i] = r[i]*b + c;
			c = r[i]/10;
			r[i] = r[i]%10;
		}
		v.pop_back();
	}
	while(size(r) > 1 && r.back() == 0)
		r.pop_back();
	return r;
}

void printMultiplication(vector<int> &v){
	while(!v.empty())
		cout<<v.back();
	cout<<endl;
}

/*
Note: LSB at Index 0, MSB at Index v.size()-1
*/
vector<int> multiply(string &sa, string &sb, int base){
	
	/*Creating input*/ 
	vector<int> a, b;
	for(int i=sa.length() - 1; i>=0; i--)
		a.pb(sa[i] - '0');
	for(int i=sb.length()-1; i>=0; i--)
		b.pb(sb[i] - '0');
	
	/*Numbers are right to left*/
	vector<int> v(size(a) + size(b));
	for(int i=0; i < size(a); i++){
		for(int j=0; j < size(b); j++){
			if(a[i] == 0)
				continue;
			v[i+j] += b[j]*a[i];
		}
	}
	
	/*For Binary*/
	for(int i=0; i<size(a) + size(b)-1; i++){
		v[i+1] += v[i]/base;
		v[i] = v[i]%base;
	}

	while(size(v) >1 && v.back() == 0)
		v.pop_back();
	
	return v;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		string a, b;
		cin>>a>>b;

		vector<int> v = multiply(a, b, 2);
		vector<int> decimal = Base2to10(v, 2);
		while(!decimal.empty()){
			cout<<decimal.back();
			decimal.pop_back();
		}
		cout<<endl;
	}
	return 0;
}