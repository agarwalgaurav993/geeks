#include <bits/stdc++.h>
#define size(s) (int)s.size()
using namespace std;

/*Returns total number of zeros in factorial of N*/
int myZeros(int n){
	int c = 0;
	int x = 5;
	while(n >= x){
		c += n/x;
		x = x*5;
	}
	return c;
}

int getSmallestNum(int zeros){
	int i=0;
	while(myZeros(i) < zeros){
		i += 5;
	}
	return i;
}

int main(){
	int T;
	cin>>T;
	while(T--){
		int N;
		cin>>N;
		cout<<getSmallestNum(N)<<endl;
	}
	return 0;
}