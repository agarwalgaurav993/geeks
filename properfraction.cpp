#include <bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;
int gcd(int x, int y)
{
	if(y == 0)
		return x;
	else
		return gcd(y, x%y);
}
int main(){
	int T;
	cin>>T;
	while(T--){
		int N;
		cin>>N;
		int i = N/2;
		int j = N - i;
		for(; i>=1; i--, j++){
			if(gcd(i, j) == 1)
				break;
		}
		cout<<i<<" "<<j<<endl;
	}
	return 0;
}