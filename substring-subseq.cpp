#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int longest(string &a, string &b){
	int mx = 0;
	vector<int> v(b.length()+1);
	vector<int> t(b.length()+1);
	for(int i=0; i<a.length(); i++){
		for(int j=0; j<b.length(); j++){
			if(a[i] == b[j]){
				t[j+1] = v[j] + 1;
			}
		}
		v = t;
	}
	for(int i=0; i<size(v); i++)
		mx = max(mx, v[i]);
	return mx;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		string a, b;
		cin>>a>>b;
		cout<<longest(a, b)<<endl;
	}
	return 0;
}