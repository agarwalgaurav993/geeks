#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;


void print(vector<vector<double> > &v){
	for(int i=0; i< size(v); i++){
		for(int j=0; j<=i ; j++)
			cout<<v[i][j]<<"\t";
		cout<<endl;
	}
}
double myAmt(int i, int j, vector<vector<double> > &v){
	if(j<0 || j>i)
		return 0;
	if(v[i][j] != -1)
		return v[i][j];
	
	double l = myAmt(i-1, j-1, v);
	double r =  myAmt(i-1, j, v);
	if(l>1)
		l = (l-1)/2;
	else
		l = 0;

	if(r>1)
		r = (r-1)/2;
	else
		r = 0;
	v[i][j] = l + r;
	return v[i][j];
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int k, i, j;
		cin>>k>>i>>j;
		vector<vector<double> > v(i, vector<double> (i, -1));
		v[0][0] = k;
		//print(v);
		if(myAmt(i-1, j-1, v) > 1)
			cout<<1<<endl;
		else
			cout<<myAmt( i-1, j-1, v)<<endl;
		//print(v);
	}
	return 0;
}