#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

void print(vector<ll> &v){
	for(int i=0; i<size(v); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}
int gKadane(vector<int> &v, int K){
	vector<ll> p(size(v), INT_MIN);
	int N = size(v);
	ll sum =0;
	for(int i=0; i<K-1; i++)
		sum += v[i];
	for(int i=K-1; i<N; i++){
		p[i] = sum;
		sum += v[i];
		sum -= v[i-K+1];
	}
	//print(p);
	int mx = INT_MIN;
	for(int i=K-1, sum=p[K-1]; i<N; i++){
		if(sum < p[i]){
			sum = p[i];
		}
		sum += v[i];
		mx = max(sum, mx);
	}
	return mx;
}


int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t;
		cin>>N;
		vector<int> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		int K;
		cin>>K;
		cout<<gKadane(arr, K)<<endl;
	}
	return 0;
}