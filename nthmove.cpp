
#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

void move(int a, int b, int c, int n, int &k){
	if(k==0 || n == 0)
		return;
	move(a, c, b, n-1, k);
	if(k == 1){
		cout<<a<<" "<<b<<endl;
	}
	k--;
	move(c, b, a, n-1, k);
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, n;
		cin>>N>>n;
		move(1, 3, 2, N, n);	
	}	

	return 0;
}