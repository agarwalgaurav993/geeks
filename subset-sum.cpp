#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int SubsetSum(int sum, vector<int> &v){
	
	vector<int> t(sum+1);
	t[0] = 1;
	for(int i=0; i<size(v); i++){
		for(int j=sum; j>=v[i]; j--){
			if(t[j - v[i]] > 0)
				t[j]+= t[j - v[i]];
		}
	}
	return t[sum];
}

int main(){
	int T;
		cin>>T;
		for(int f=1; f<=T; f++){
			int N, t, sum;
			cin>>N;
			vector<int> arr;
			for(int i=0; i<N; i++){
				cin>>t;
				arr.push_back(t);
			}
			cin>>sum;
			cout<<SubsetSum(sum, arr)<<endl;
		}	
	return 0;
}

