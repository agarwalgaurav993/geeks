#include <bits/stdc++.h>

using namespace std;

/*Part - 1*/
/*This Function would Check if Ith Index accounts for Yes/No as per the given target*/
bool checkYesNo(int i, int target, vector<int> & ar){
    /*Currently This function would return True if ar[i] can be lower bound of target element*/
    if(ar[i] <= target)
        return true;
    else
        return false;
}
/*B-Search to Return Last Yes from the array of [Y, Y, Y, Y, Y, Y, N]*/
/*Function returns index of the Last Yes in case Yes is present, else returns -1*/
/*Note: Function to Select "m"*/
int BSearch(int s, int e, int target, vector<int>& ar){
        if(s > e)
            return -1;
        if(s == e){
            if(checkYesNo(s, target, ar))
                return ar[s];
            else
                return -1;
        }
        int m = s + (e - s + 1)/2;
        if(checkYesNo(m, target, ar)){
            s = m;
            return  BSearch(s, e, target, ar);
        }
        else{
            e = m - 1;
            return BSearch(s, e, target, ar);
        }
}

int main(){
    int T;
    cin>>T;
    while(T--){
        int N, X;
        cin>>N>>X;
        vector<int> v(N);
        for(int i=0; i<N; i++)
            cin>>v[i];
        cout<<BSearch(0, v.size()-1, X, v)<<endl;
        
    }
    return 0;
}