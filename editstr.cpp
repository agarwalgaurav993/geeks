#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;
void print(vector<vector<int> > &v){
	for(int i=0; i<size(v); i++){
		for(int j=0; j<size(v[i]); j++)
			cout<<v[i][j]<<" ";
		cout<<endl;
	}
	
}
/*Longest Uncommon Subsequence*/
int minOp(string &a, string &b){
	vector<vector<int> > v(b.length()+1, vector<int> (a.length()+1));
	for(int i=0, j=0; i<size(v) && j<size(v[i]); j++)
		v[i][j] = j;

	for(int i=0, j=0; i<size(v) && j<size(v[i]); i++)
		v[i][j] = i;
	
	for(int i=1; i<size(v); i++)
		for(int j=1; j<size(v[i]); j++){
			if(b[i-1] == a[j-1])
				v[i][j] = v[i-1][j-1];
			else
				v[i][j] = 1 + min(v[i-1][j], min(v[i][j-1], v[i-1][j-1]));
		}
		//print(v);
	return v[b.length()][a.length()];
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int m, n;
		cin>>m>>n;
		string a, b;
		cin>>a>>b;
		cout<<minOp(a, b)<<endl;
	}
	return 0;
}
