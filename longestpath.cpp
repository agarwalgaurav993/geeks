#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int lp(int i, int j, vector<vector<int> > &v, vector<vector<int> > &p, int r){
	if(i<0 || j<0 || i >= size(v) || j >= size(v))
		return 0;
	if(r != p[i][j])
		return 0;
	if(v[i][j] == -1)
		v[i][j] = 1 + max(max(lp(i-1, j, v, p, r+1), lp(i+1, j, v, p, r+1)), max(lp(i, j-1, v, p, r+1), lp(i, j+1, v, p, r+1)));
	return v[i][j];
}

int maxlength(vector<vector<int> > &v){
	vector<vector<int> > p(size(v), vector<int> (size(v), -1));

	for(int i=0; i<size(p); i++){
		for(int j=0; j<size(p); j++){
			if(p[i][j] == -1)
				lp(i, j, p, v, v[i][j]);
		}
	}
	int mx = 0;
	for(int i=0; i<size(p); i++){
		for(int j=0; j<size(p); j++){
			mx = max(mx, p[i][j]);
		}
	}
	return mx;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<vector<int> > v(N, vector<int> (N));
		for(int i=0; i<N; i++)
			for(int j=0; j<N; j++)
				cin>>v[i][j];
		cout<<maxlength(v)<<endl;
	}
	return 0;
}