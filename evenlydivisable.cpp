#include <bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;
int gcd(int x, int y){
	if(y == 0)
		return x;
	return gcd(y, x%y);
}
int main(){
	int T;
	cin>>T;
	while(T--){
		int N;
		cin>>N;
		int r = 1;
		for(int i=1; i<=N; i++)
			r = (r*i)/gcd(r, i);
		
		for(int i = 1; i<=N; i++){
			if(((r/i)&1) == 1){
				r = r*2;
				break;
			}
		}
		cout<<r<<endl;
	}	
	return 0;
}