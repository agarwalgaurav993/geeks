#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

bool isBleak(int n){
	for(int i=1; i<n; i++){
		if(i+__builtin_popcount(i) == n)
			return false;
	}
	return true;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		cout<<isBleak(N)<<endl;
	}
	return 0;
}