#include <bits/stdc++.h>
using namespace std;

int getMax(vector<int> &a, vector<int> &b){
	vector<int> v(a.size() + b.size());
	vector<int> p(a.size() + b.size());
	int i=0, j=0, k=0;
	while(i<a.size() || j<b.size()){
		if(i<a.size() && j<b.size() && a[i] == b[j]){
			v[k] = a[i];
			p[k] = 3;
			i++;
			j++;
		}
		else if(j == b.size() || (i<a.size() && a[i] < b[j])){
			v[k] = a[i++];
			p[k] = 1;
		}
		else{
			v[k] = b[j++];
			p[k] = 2;
		}
		//cout<<p[k]<<" ";
		k++;
	}
	//cout<<endl;
	int sumi = 0, sumj = 0;
	int sum = 0;
	for(int i=0; i<k; i++){
		if(p[i] == 1){
			sumi += v[i];
		}
		else if(p[i] == 2){
			sumj += v[i];
		}
		else{
			sum += max(sumi, sumj) + v[i];
			sumi = sumj = 0;
		}
	}
	sum += max(sumi, sumj);
	return sum;
}
int main(){
	int T;
	cin>>T;
	while(T--){
		int N, M;
		cin>>N>>M;
		vector<int> a(N);
		vector<int> b(M);
		for(int i=0; i<N; i++)
			cin>>a[i];
		for(int i=0; i<M; i++)
			cin>>b[i];
		cout<<getMax(a, b)<<endl;
	}	
	return 0;
}