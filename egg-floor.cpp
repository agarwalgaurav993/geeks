#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;
void print(vector<vector<int> > &v){
	for(int i=0; i<size(v); i++){
		for(int j=0; j<size(v[i]); j++)
			cout<<v[i][j]<<" ";
		cout<<endl;
	}
}
int EggFloor(int K, int N, vector<vector<int> > &v){
	if( K == 1 )
		return N;
	if(v[K][N] != INT_MAX)
		return v[K][N];
	for(int f=1; f<=N; f++){
		v[K][N] = min(v[K][N], 1 + max(EggFloor(K-1, f-1, v), EggFloor(K, N-f, v)));
	}

	return v[K][N];
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, K;
		cin>>K>>N;
		vector<vector<int> > v(K+1, vector<int> (N+1, INT_MAX));
		for(int i=0; i<size(v); i++)
			v[i][0] = 0;
		cout<<EggFloor(K, N, v)<<endl;
		//print(v);
	}
	return 0;
}