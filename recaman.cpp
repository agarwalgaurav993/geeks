#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

void Recaman(vector<int> &v, vector<bool> &p, int n){
	
	if(v[n-1] - n > 0 && !p[v[n-1]-n])
		v[n] = v[n-1] - n;
	else
		v[n] = v[n-1] + n;
	p[v[n]] = true;

}

int main(){
	vector<int> v(100);
	vector<bool> p(1000);
	v[0] = 0;
	for(int i=1; i<100; i++)
		Recaman(v, p, i);
	
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int n;
		cin>>n;
		for(int i=0; i<n; i++)
			cout<<v[i]<<" ";
		cout<<endl;
	}

	return 0;
}