#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int longestAP(vector<int> &v){
	vector<int> t(20001);
	int m = 0;
	for(int i=0; i<size(v); i++){
		for(int j=i+1; j <size(v); j++){
			int in = v[j] - v[i] + 1000;
			t[in]++;
			m = max(t[in], m);
		}
	}

	return m+1;
}

/*int longestAP(vector<int> &v){
	if(size(v) < 3)
		return size(v);
	vector<vector<int> > t(v.back() - v[0] +1, vector<int>(size(v), 1));
	int m = 0;
	for(int j=1; j<size(v); j++){
		for(int i=0; i<j; i++){
			int d = abs(v[j] - v[i]);
			t[d][j] = max(t[d][i] + 1, t[d][j]);
			m = max(m, t[d][j]);
		}
	}
	return m;
}*/
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t;
		cin>>N;
		vector<int> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		cout<<longestAP(arr)<<endl;
	}
	return 0;
}
