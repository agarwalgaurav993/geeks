#include <bits/stdc++.h>

using namespace std;

int main(){
	int T;
	cin>>T;
	while(T--){
		int M, N;
		cin>>M>>N;
		vector<int> c(2000);
		vector<int> a(M);
		vector<int> b(N);
		for(int i=0; i<M; i++){
			cin>>a[i];
			c[a[i]]++;
		}
		for(int i=0; i<N; i++)
			cin>>b[i];
		vector<int> r(M);
		int k;
		for(int i=0, k=0; i<N; i++){
			while(c[b[i]]){
				cout<<b[i]<<" ";
				c[b[i]]--;
			}
		}
		sort(a.begin(), a.end());
		for(int i=0; i<M; i++){
			if(c[a[i]])
				cout<<a[i]<<" ";
		}
		cout<<endl;

	}
	return 0;
}