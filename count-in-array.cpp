	#include <bits/stdc++.h>
	#define size(s) (int)s.size()
	#define pb push_back
	#define ll long long
	#define mp make_pair
	#define D 1000000007
	using namespace std;

	int count(int N, int M){
		if(N<1)
			return 0;
		vector<vector<int> > v(N+1, vector<int> (M+1));
		int i=1;
		if(i<size(v))
			for(int j=1; j < size(v[i]); j++)
				v[i][j] = 1;
		for(int i=2; i <= N; i++){
			for(int j=1; j<=M; j++){
				for(int k=1; k<=M; k++)
					if(j%k == 0 || k%j == 0)
						v[i][j] = (v[i][j] +  v[i-1][k])%D;
			}
		}
		int sum = 0;
		for(int j=1; j<=M; j++){
			sum = (sum + v[N][j])%D;
		}
		return sum;
	}


	int main(){
		int T;
		cin>>T;
		for(int f=1; f<=T; f++){
			int N, M;
			cin>>N>>M;
			cout<<count(N, M)<<endl;
		}
		return 0;
	}