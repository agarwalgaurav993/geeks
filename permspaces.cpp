#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

void recur(string &s, int i, string &str){
	if(i == str.length()){
		cout<<"("<<s<<")"<<endl;
		return;
	}

	if((i&1) == 0){
		s.pb(str[i]);
		recur(s, i+1, str);
		s.pop_back();
	}
	else{
		s.pb(str[i]);
		recur(s, i+1, str);
		s.pop_back();
		recur(s, i+1, str);
	}
	
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		string s;
		cin>>s;
		string ns;
		for(int i=0; i<s.length(); i++)
		{
			ns.pb(s[i]);
			ns.pb(' ');
		}
		ns.pop_back();
		string temp;
		recur(temp, 0, ns);
	}
	return 0;
}