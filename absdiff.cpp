#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int maxAbsDiff(vector<int> &v){
	int sum = 0;
	for(int i=0; i<size(v); i++)
		sum += v[i];
	int csum = 0;
	int mx = 0;
	for(int i=0; i<size(v); i++){
		csum += v[i];
		mx = max(mx, abs((sum - csum) - csum));
	}
	return mx;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t;
		cin>>N;
		vector<int> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		cout<<maxAbsDiff(arr)<<endl;
	}
	return 0;
}