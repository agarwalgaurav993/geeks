#include<bits/stdc++.h>
using namespace std;
bool cmp(pair<int, int> &a, pair<int, int> &b){
	if(a.first != b.first )
		return a.first < b.first;
	return a.second < b.second;
}
int main()
 {
	//code
	int T;
	cin>>T;
	while(T--){
	    int N;
	    map <int, int> sx;
	    map <int, int> sy;
	    cin>>N;
	    vector<pair<int, int> > v;
	    for(int i=1; i<=N; i++){
	        int x, y;
	        cin>>x>>y;
	        v.push_back(make_pair(x, y));
	    }
	    sort(v.begin(), v.end(), cmp);
	    int k=0;
	    for(int i=0; i<N; i++){
	    	if(v[i].first != v[k].first || v[i].second != v[k].second){
	    		k++;
	    		v[k].first = v[i].first;
	    		v[k].second = v[i].second;
	    	}
	    }
	    for(int i=0; i<=k; i++){
	        int x = v[i].first;
	        int y = v[i].second;
	        if(sx.find(x) == sx.end())
	            sx[x] = 0;
	        sx[x] = sx[x] + 1;
	        
	        if(sy.find(y) == sy.end())
	            sy[y] = 0;
	        sy[y] = sy[y] + 1;
	    }
	    map<int, int>::iterator it;
	    long long t = 0;
	    for(it =sx.begin(); it != sx.end(); ++it){
	       t = t + (it->second*(it->second - 1))/2;
	    }
	    for(it =sy.begin(); it != sy.end(); ++it){
	        t += (it->second * (it->second -1))/2;
	    }
	    cout<<t<<endl;
	}
	return 0;
}