#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

bool hamiltonian(vector<vector<int> >&graph, int s, vector<bool> &visited, int left){
	if(left == 0)
		return true;
	if(visited[s])
		return false;
	visited[s] = true;
	for(int i=0; i< size(graph[s]); i++){
		if(hamiltonian(graph, graph[s][i], visited, left-1))
			return true;
	}
	visited[s] = false;
	return false;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, M;
		cin>>N>>M;
		vector<vector<int> > graph(N+1);
		for(int i=0; i<M; i++){
			int s, d;
			cin>>s>>d;
			graph[s].pb(d);
			graph[d].pb(s);
		}
		vector<bool> visited(N+1);
		bool b = false;
		for(int i=1; i<=N && !b; i++)
			b = hamiltonian(graph, i, visited, N);
		cout<<b<<endl;
	}
	return 0;
}