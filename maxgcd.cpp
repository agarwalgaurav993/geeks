#include<bits/stdc++.h>
#define MAX 100001
using namespace std;

int main()
 {
	//code
	int T;
	cin>>T;
	while(T--){
	    int N;
	    cin>>N;
	    vector<int> v(MAX);
	    for(int i=0; i<N; i++)
	    {
	        int t;
	        cin>>t;
	        v[t] += 1;
	    }
	    int mx = 1;
	    for(int i=2; i<MAX; i++){
	        int c = 0;
	        for(int j=1; i*j > 0 && i*j<MAX; j++){
	            if(v[i*j] > 0)
	                c+= v[i*j];
	        }
	        if(c > 1)
	            mx = max(mx, i);
	    }
	    cout<<mx<<endl;
	}
	return 0;
}