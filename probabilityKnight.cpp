#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long				
#define mp make_pair
using namespace std;

bool inside(int x, int y, int N){
	return (x>=0 && y>=0 && x<N && y<N);
}
double gp(int N, int K, int X, int Y){
	
	int dx[] = { 1, 2, 2, 1, -1, -2, -2, -1 };
	int dy[] = { 2, 1, -1, -2, -2, -1, 1, 2 };
	double ar[N][N][K+1];	/*It stores Probability to be inside matrix*/
	for(int i=0; i<N; i++)
		for(int j=0; j<N; j++)
			ar[i][j][0] = 1.0;

	for(int k=1; k<=K; k++){
		for(int i=0; i<N; i++){
			for(int j=0; j<N; j++){
				ar[i][j][k] = 0.0;
				for(int m=0; m<8; m++){
					int ni = i + dx[m];
					int nj = j + dy[m];
					if(inside(ni, nj, N))
						ar[i][j][k] += ar[ni][nj][k-1];
				}
				ar[i][j][k] = ar[i][j][k]/8.0;
			}
		}
	}
	return ar[X][Y][K];
}

int main(){ 
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, X, Y, K;
		cin>>N>>X>>Y>>K;
		cout<<fixed<<setprecision(6)<<gp(N, K, X, Y)<<endl;
	}
	return 0;
}