#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;
vector<vector<int> > t;
void print(vector<int> &v){
	cout<<"(";
	for(int i=0; i<size(v)-1; i++)
		cout<<v[i]<<" ";
	cout<<v.back();
	cout<<")";
}
void recur(vector<int> &v, int pos, vector<int> &r, int csum){
	if(csum == 0){
		t.pb(r);
		return;
	}
	if(pos >= size(v) || csum < 0 )
		return;
	int e = v[pos];
	while(pos < size(v)){
		e = v[pos];
		if(csum - v[pos] < 0)
			return;
		r.pb(v[pos]);
		recur(v, pos + 1, r, csum - v[pos]);
		r.pop_back();
		while(pos < size(v) && v[pos] == e)
			pos++;	

	}
	

}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, sum;
		t.clear();
		cin>>N;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		cin>>sum;
		
		sort(v.begin(), v.end());
		
		vector<int> r;
		recur(v, 0, r, sum);
		if(!t.empty()){
			for(int i=0; i<size(t); i++){
				print(t[i]);
			}
		}
		else
			cout<<"Empty";
		cout<<endl;
	}
	return 0;
}