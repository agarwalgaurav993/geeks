#include <bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;
void print(vector<int> &v){
	for(int i=0; i<v.size(); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, K;
		cin>>N>>K;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		sort(v.begin(), v.end());
		int prev = 0;
		int m = INT_MAX;
		for(int i=0; i<N; i++){
			int sum = 0;
			for(int j=i+1; j<N; j++){
				if(v[j] - v[i] >=K)
					sum += v[j] - v[i] - K;
			}
			m = min(m, sum + prev);
			prev += v[i];
		}
		
		cout<<m<<endl;
	}
	return 0;
}