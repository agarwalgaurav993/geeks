#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;


vector<int> Base2to10(vector<int> &v, int b){
	/*Note : Number in base 2 is from Right to Left i.e, LSB at 0*/
	while(size(v) > 1 && v.back() == 0)
		v.pop_back();

	int s = ceil((float)size(v)*log(b));
	vector<int> r(s);
	int c =0;
	while(!v.empty()){
		c = v.back();
		for(int i=0; i<s; i++){
			r[i] = r[i]*b + c;
			c = r[i]/10;
			r[i] = r[i]%10;
		}
		v.pop_back();
	}
	while(size(r) > 1 && r.back() == 0)
		r.pop_back();
	return r;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<int> v(N, 1);
		vector<int> r = Base2to10(v, 2);
		while(!r.empty()){
			cout<<r.back();
			r.pop_back();
		}
		cout<<endl;
	}
	return 0;
}