#include <bits/stdc++.h>
#define MAX 1000000
using namespace std;

vector<int> sp;
set<int> s;
vector<int> primes;
vector<int> v(MAX, 1);
void seive(int start, int end){
	//int factor = max(start, MAX);
	fill(v.begin(), v.end(), 1);
	for(int i=0; i<primes.size(); i++)
		for(int j=start/primes[i]; j<=end/primes[i]; j++)
			if(primes[i]*j>=start)
				v[(primes[i]*j) - start] = 0;
	for(int i=start; i < end; i++){
		if(v[i - start] && i > 1){
			primes.push_back(i);
			for(int p=i; p<=end/i; p++){
				v[(i*p)- start] = 0;
			}
		}
	}
}
void segmented_seive(int range){
	int s = 0, e = MAX;
	while(e <= range){
		seive(s, e);
		cout<<primes.size()<<endl;
		s = e;
		e += MAX;
	}
}
void super_primes(){
	
	for(int i=1; i<primes.size(); i++){
		if(primes[i]-2 == primes[i-1]){
			sp.push_back(primes[i]);
		}
	}
}


/*This Function would Check if Ith Index accounts for Yes/No as per the given target*/
bool checkYesNo(int i, int target, vector<int> & ar){
    /*Currently This function would return True if ar[i] can be lower bound of target element*/
    if(ar[i] <= target)
        return true;
    else
        return false;
}
/*B-Search to Return Last Yes from the array of [Y, Y, Y, Y, Y, Y, N]*/
/*Function returns index of the Last Yes in case Yes is present, else returns -1*/
/*Note: Function to Select "m"*/
int BSearch(int s, int e, int target, vector<int>& ar){
        if(s > e)
            return -1;
        if(s == e){
            if(checkYesNo(s, target, ar))
                return s;
            else
                return 0;
        }
        int m = s + (e - s + 1)/2;
        if(checkYesNo(m, target, ar)){
            s = m;
            return  BSearch(s, e, target, ar);
        }
        else{
            e = m - 1;
            return BSearch(s, e, target, ar);
        }
}
int main(){
	segmented_seive(10000000);
	super_primes();
	/*int T;
	cin>>T;
	while(T--){
		int N;
		cin>>N;
		cout<<BSearch(0, sp.size()-1, N, sp)<<endl;
	}*/
	return 0;
}