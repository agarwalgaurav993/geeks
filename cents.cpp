#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int centWays(vector<int> &c, int amt){
	vector<ll> v(amt+1);
	v[0] = 1;
	for(int i=0; i<size(c); i++){
		for(int j=c[i]; j<=amt; j++){
			if(v[j - c[i]] != 0){
				v[j] += v[j - c[i]];
			}
		}
	}
	return v[amt];
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t;
		cin>>N;
		vector<int> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		int amt;
		cin>>amt;
		cout<<centWays(arr, amt)<<endl;
	}
	return 0;
}