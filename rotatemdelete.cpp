#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

void print(vector<int> &v){
	for(int i=0; i<size(v); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		int k=1;
		while(size(v) > 1){
			/*Rotate*/
			vector<int> t;
			t.pb(v[size(v)-1]);
			for(int i=0; i< size(v) - 1; i++)
				t.pb(v[i]);
			//print(t);
			/*Delete*/
			int pos = max(0, size(t) - k);
			t.erase(t.begin() + pos);
			v = t;
			k++;
		}
		if(!v.empty())
			cout<<v.back()<<endl;
	}
	return 0;
}