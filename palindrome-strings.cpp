#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
#define M 1000000007
using namespace std;

ll ways(int N, int k){
	bool f = false;
	ll sum = 0;
	ll p = 1;
	for(int i=1; i<=N; i++){
		if(!f){
			p = (p*k)%M;
			k--;
			f = true;
		}
		else
			f = false;
		sum = (sum + p)%M;
	}
	return sum;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, K;
		cin>>N>>K;
		cout<<ways(N, K)<<endl;
	}
	return 0;
}