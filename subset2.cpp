#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

vector<vector<int> > recur(vector<int> &v){
	vector<vector<int> > t;
	vector<int> r;
	t.pb(r);
	sort(v.begin(),v.end());
	int p = -1;
	for(int i=0; i<size(v); i++){
		int j = i;
		if(j<size(v) && v[j] == p){
			r.pb(v[j]);
			j++;
		}
		for(; j<size(v); j++){
			r.pb(v[j]);
			t.pb(r);
		}
		r.clear();
		p = v[i];
	}
	return t;
}

int main(){
	
	return 0;
}