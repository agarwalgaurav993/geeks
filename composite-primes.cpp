#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
#define MAX 1000000
using namespace std;

void seive(int start, int end, vector<int> &primes){
	vector<int> v(MAX, 1);
	for(int i = 0; i < size(primes); i++)
		for(int j = start/primes[i]; j <= end/primes[i]; j++)
			if( primes[i]*j >= start)
				v[ (primes[i]*j) - start ] = 0;
	for(int i = start; i < end; i++){
		if(v[ i - start ] && i > 1){
			primes.push_back(i);
			for(int p = i; p <= end/i; p++){
				v[ (i*p)- start ] = 0;
			}
		}
	}
}

/*
Input: range - It should be in multiple of 10^5
Output : prime numbers
*/
vector<int> segmented_seive(int range){
	vector<int> primes;
	int s = 0, e = MAX;
	while(e <= range){
		seive(s, e, primes);
		//cout<<primes.size()<<endl;
		s = e;
		e += MAX;
	}
	return primes;
}

int b_search(int s, int e, int t, vector<int> &p){
	if(s == e){
		if(p[s] <= t)
			return s;
		return -1;
	}
	int m = s + (e-s+1)/2;
	if(p[m] <= t)
		return b_search(m, e, t, p);
	else
		return b_search(s, m-1, t, p);
}


int lr(int l, int r, vector<int> &p){
	int i = b_search(0, size(p)-1, l, p);
	int ii = b_search(0, size(p)-1, r, p);
	if(i == -1)
		i = 0;
	else if(p[i] != l)
		i++;

	if(ii == -1)
		ii=0;
	else
		ii++;


	
	/*Primes*/
	int pp = ii - i;
	int npp = r-l+1 - pp;
	if(l == 1)
		npp--;
	//cout<<pp<<" "<<npp<<endl;
	return npp - pp;

}
int main(){
	vector<int> p = segmented_seive(10000000);
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int l, r;
		cin>>l>>r;
		cout<<lr(l, r, p)<<endl;
	}
	return 0;
}