#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

/*i = row ; j = column*/
bool safe(vector<int> &q, int i, int j){

	for(int k=1; k<j; k++){
		if(q[k] == i || abs(q[k] - i) == abs(k - j))
			return false;
	}
	return true;
}
vector<vector<int> > t;
bool nqueen(int n, int j, vector<int> &q){

	if(j == n+1)
	{
		t.pb(q);
		return true;
	}
	for(int i=1; i<=n; i++){
		if(safe(q, i, j)){
			q[j] = i;
			nqueen(n, j+1, q);
		}
	}
}

void print(vector<int> &v){
	cout<<"[";
	for(int i=1; i<size(v); i++)
		cout<<v[i]<<" ";
	cout<<"]";
}

void printer(){

	for(int i=0; i<size(t); i++){
		print(t[i]);
		cout<<" ";
	}
	cout<<endl;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		t.clear();
		int N;
		cin>>N;
		vector<int> q(N+1);
		nqueen(N, 1, q);
		if(t.empty())
			cout<<-1<<endl;
		else
			printer();
	}
	return 0;
}