#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

ll total = 0;
ll k;

ll gcd(ll x, ll y){
	if(y == 0)
		return x;
	return gcd(y, x%y);
}

void generateSubsets(vector<int> &v, int i, ll p, int c){
	if(i == size(v)){
		/*Operation on Subset*/
		if((c&1) == 0 && c != 0){
			total = total - k/p;
		}
		else if((c&1) == 1){
			total = total + k/p;
		}
		return;
	}
	generateSubsets(v, i+1, p, c);
	p = p*v[i]/gcd(p, v[i]);
	generateSubsets(v, i+1, p, c+1);
	//p = p/v[i];
}



int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t;
		cin>>N;
		vector<int> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		cin>>k;
		sort(arr.begin(), arr.end());
		vector<int> v;
		for(int i=0; i<size(arr);){
			v.pb(arr[i]);
			while(arr[i] == v.back())
				i++;
		}
		ll p = 1;
		total = 0;
		generateSubsets(v, 0, p, 0);
		cout<<total<<endl;
	}
	return 0;
}