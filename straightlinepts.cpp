#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define len(s) (int)s.length()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int countPts(int X[], int Y[], int N, int a, int b, int c, int d){
	int count = 0;
	for(int i=0; i<N; i++){
		long long v1 = (long long)(Y[i] - c)*(long long)a;
	    long long v2 = (long long)(X[i] - d)*(long long)b;
		//cout<<v1<<" "<<v2<<endl;
		if( v1 == v2)
			count++;
	}
	return count;
}
int maxPoints(int X[], int Y[],int N) {
     //Your code here
	int mx = 0;
	for(int i=0; i<N; i++){
		for(int j=i+1; j<N; j++){
			int a = X[i] - X[j];
			int b = Y[i] - Y[j];
			int c = Y[j];
			int d = X[j];
			mx = max(mx, countPts(X, Y, N, a, b, c, d));
		}
	}
	return mx;
}
void print(int ar[], int N){
	for(int i=0; i<N; i++)
		cout<<ar[i]<<" ";
	cout<<endl;
}
int main(){
	int N;
	cin>>N;
	int X[N];
	int Y[N];
	for(int i=0; i<N; i++){
		cin>>X[i];
	}	
	for(int i=0; i<N; i++)
		cin>>Y[i];
	int i, j;
	for(i=1, j=0 ; i<N; i++){
		if(X[i] != X[j] || Y[i] != Y[j])
			j++;
	}
	cout<<maxPoints(X, Y, j+1)<<endl;
	return 0;
}