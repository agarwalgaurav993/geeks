#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;


void rearrange(vector<int> &v){

	for(int i=0; i<size(v); i++)
		v[i] = v[i]*1000;

	for(int i=0; i<size(v); i++)
		v[i] = v[i] + v[v[i]/1000]/1000;

	for(int i=0; i<size(v); i++)
		v[i] = v[i]%1000;
}

void print(vector<int> &v){
	for(int i=0; i<size(v); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		rearrange(v);
		print(v);
	}
	return 0;
}