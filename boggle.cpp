#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;


set<string> getCanProduced(vector<string> &v, vector<int> &c){

	set<string> r;

	for(int i=0; i<size(v); i++){
		vector<int> t(256);
		for(int j=0; j<v[i].length(); j++)
			t[v[i][j]]++;
		bool f = true;
		for(int i=0; i<256 && f; i++)
			if(c[i] < t[i])
				f = false;
		if(f)
			r.insert(v[i]);
	}
	
	return r;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<string> v;
		for(int i=0; i<N; i++){
			string s;
			cin>>s;
			v.pb(s);
		}
		int x, y;
		cin>>x>>y;
		char t;
		vector<int> c(256);
		//cout<<"OK";
		for(int i=0; i<x*y; i++){
			cin>>t;
			//cout<<i<<" "<<t-'A'<<endl;
			c[t]++;
		}
		
		set<string> res = getCanProduced(v, c);
		set<string> :: iterator it;
		for(it = res.begin(); it != res.end(); it++)
			cout<<*it<<" ";
		if(res.empty())
			cout<<-1;
		cout<<endl;
	}	
	return 0;
}