#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;



struct Node
{
	int key;
	struct Node *left, *right;
};

// your task is to complete the Function
// Note: Function should return median of the BST

int myCount(Node *root){
	if(!root)
		return 0;
	else
		return myCount(root->left) + 1 + myCount(root->right);
}
int i;
int getIth(int p, Node *root, int h){
	if(!root)
		return 0;
	int lcnt = getIth(p, root->left, h);
	if(lcnt + 1 + p == h)
		i = root->key;
	int rcnt = getIth(lcnt + 1 + p, root->right, h);
	return lcnt + 1 + rcnt;
	
}
int findMedian(struct Node *root)
{
      //Code here
	int total = myCount(root);
	if(total == 0)
		return 0;
	int median = 0;
	if((total&1) == 0){
		getIth(0, root, total/2);
		median += i;
		getIth(0, root, total/2 +1);
		median += i;
		return (median>>1);
	}
	else{
		getIth(0, root, total/2 +1);
		median = i;
		return median; 	
	}
	

}
int main(){
	
	return 0;
}