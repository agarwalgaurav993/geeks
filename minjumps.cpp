#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;
int minJumps(vector<int> &s){
	if(size(s) < 1)
		return 0;
	vector<int> v(size(s), INT_MAX);
	v[0] = 0;
	for(int i=0; i<size(s); i++){
		if(v[i] != INT_MAX)
		for(int j=i+1 ; j<min(i+s[i]+1, size(v)); j++){
			v[j] = min(v[j], v[i]+1);
			cout<<j<<endl;
		}
	}
	if(v.back() == INT_MAX)
		return -1;
	return v.back();
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t;
		cin>>N;
		vector<int> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		cout<<minJumps(arr)<<endl;
	}
	return 0;
}