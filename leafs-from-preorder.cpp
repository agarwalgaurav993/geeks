#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

bool check(int x, int t){
	return x<t;
}

int b_search(int s, int e, int t, vector<int> &ar){
	if(s == e){
		if(check(ar[s], t))
			return s;
		return -1;
	}

	int m = s + (e-s+1)/2;
	if(check(ar[m], t))
		return b_search(m, e, t, ar);
	else
		return b_search(s, m-1, t, ar);
}

void getChild(vector<int> &v, int s, int e){
	if(s>e)
		return;
	if(s == e){
		cout<<v[s]<<" ";
		return;
	}
	int m = b_search(s+1, e, v[s], v);
	if(m != -1){
		getChild(v, s+1, m);
		getChild(v, m+1, e);
	}
	else
		getChild(v, s+1, e);
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t;
		cin>>N;
		vector<int> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		getChild(arr, 0, size(arr)-1);
		cout<<endl;
	}
	return 0;
}