#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

string t;

void recur(string &s, int i, int k){
	//cout<<s<<endl;
	if(i == s.length() || k <= 0){
		if(t < s)
			t = s;
		return;
	}
	int mx = s[i];
	for(int j=s.length()-1; j>i; j--)
		mx = max(mx, (int)s[j]);
	//cout<<(char)mx<<endl;
	if(mx != s[i]){
		for(int j=s.length()-1; j>i; j--){
			if(s[j] == mx){
				char c = s[i];
				s[i] = s[j];
				s[j] = c;
				recur(s, i+1, k-1);
				c = s[i];
				s[i] = s[j];
				s[j] = c;
			}

		}
	}
	recur(s, i+1, k);
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		string s;
		int K;
		
		cin>>K;
		cin>>s;
		t = s;
		recur(s, 0, K);
		cout<<t<<endl;
	}
	return 0;
}