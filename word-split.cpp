#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;
bool m = false;
struct Trie{
	bool f;
	vector<Trie*> v;
	Trie(): f(false), v(vector<Trie*> (26)) {}
};

void Insert(Trie *root, string &s, int i){
	if(i == s.length()){
		root->f = true;
		return;
	}

	int j = s[i] - 'a';
	if(root->v[j] == NULL)
		root->v[j] = new Trie;
	return Insert(root->v[j], s, i+1);
}

void print(vector<string> &v){
	cout<<"(";
	for(int i=0; i<size(v)-1; i++)
		cout<<v[i]<<" ";
	if(!v.empty())
		cout<<v.back();
	cout<<")";
}

void split(string &s, int i, vector<string> &v, Trie *root){
	if(i == s.length()){
		m = true;
		print(v);
		return;
	}
	Trie *t = root;
	for(int j=i; j<s.length(); j++){
		t = t->v[s[j] - 'a'];
		if(!t)
			return;
		if(t->f){
			v.pb(s.substr(i, j-i+1));
			split(s, j+1, v, root);
			v.pop_back();
		}
	}
}

int main(){
	
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		m = false;
		Trie *root = new Trie;	
		int N;
		cin>>N;
		for(int i=0; i<N; i++){
			string s;
			cin>>s;
			Insert(root, s, 0);
		}
		string s;
		cin>>s;
		vector<string> v;
		split(s, 0, v, root);
		if(m)
			cout<<endl;
		else
			cout<<"Empty\n";
	}
	return 0;
}