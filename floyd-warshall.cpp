#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

/*returns All pairs shortest path*/
void FloydWarshall(vector<vector<int> > &graph){
	//vector<vector<int> > v(size(graph), vector<int> (size(graph)));
	//vector<vector<int> > t;
	for(int k=0; k< size(graph); k++){
		for(int i=0; i<size(graph); i++){
			for(int j=0; j<size(graph); j++){
				graph[i][j] = min(graph[i][j], graph[i][k] + graph[k][j]);
			}
		}
	}
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<vector<int> > graph(N, vector<int> (N));
		for(int i=0; i<N; i++)
			for(int j=0; j<N; j++)
				cin>>graph[i][j];
		FloydWarshall(graph);

		for(int i=0; i<N; i++){
			for(int j=0; j<N; j++)
				cout<<graph[i][j]<<" ";
		}
		cout<<endl;

	}
	return 0;
}