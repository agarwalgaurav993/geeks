
#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
#define MAX 1000000
using namespace std;

void seive(int start, int end, vector<int> &primes){
	vector<int> v(MAX, 1);
	for(int i = 0; i < size(primes); i++)
		for(int j = start/primes[i]; j <= end/primes[i]; j++)
			if( primes[i]*j >= start)
				v[ (primes[i]*j) - start ] = 0;
	for(int i = start; i < end; i++){
		if(v[ i - start ] && i > 1){
			primes.push_back(i);
			for(int p = i; p <= end/i; p++){
				v[ (i*p)- start ] = 0;
			}
		}
	}
}

/*
Input: range - It should be in multiple of 10^5
Output : prime numbers
*/
vector<int> segmented_seive(int range){
	vector<int> primes;
	int s = 0, e = MAX;
	while(e <= range){
		seive(s, e, primes);
		//cout<<primes.size()<<endl;
		s = e;
		e += MAX;
	}
	return primes;
}

ll factor(ll n, vector<int> &p){
	ll mx = 1;
	for(int i=0; i<size(p) && p[i] <= n; i++){
		while(n%p[i] == 0){
			n = n/p[i];
			mx = max(mx, (ll)p[i]);
		}
	}
	return max(mx, n);
}

int main(){
	vector<int> p = segmented_seive(MAX);
	cout<<size(p)<<endl;
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		ll N;
		cin>>N;
		cout<<factor(N, p)<<endl;
	}
	return 0;
}