#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int gcd(int x, int y){
	if( y == 0)
		return x;
	return gcd(y, x%y);
}

vector<vector<int> > fillStates(int c1, int c2){
	vector<vector<int> > v(c1+1, vector<int>(1+c2, INT_MAX));
	queue<pair<int, int> > q;
	v[0][0] = 0;
	q.push(mp(0, 0));
	while(!q.empty()){
		pair<int, int> p = q.front();
		q.pop();
		int x = p.first, y = p.second;
		if(v[0][y] > v[x][y] + 1){
			v[0][y] = v[x][y] + 1;
			q.push(mp(0, y));
		}
		if(v[x][0] > v[x][y] + 1){
			v[x][0] = v[x][y] + 1;
			q.push(mp(x, 0));
		}
		int d = min(c2 - y, x);
		if(v[x-d][y+d] > v[x][y] + 1){
			v[x-d][y+d] = v[x][y] + 1;
			q.push(mp(x-d, y+d));
		}
		d = min(c1 - x, y);
		if(v[x+d][y-d] > v[x][y]+1){
			v[x+d][y-d] = v[x][y] +1;
			q.push(mp(x+d, y-d));
		}
		if(v[c1][y] > v[x][y] + 1){
			v[c1][y] = v[x][y] + 1;
			q.push(mp(c1, y));
		}
		if(v[x][c2] > v[x][y] + 1){
			v[x][c2] = v[x][y] + 1;
			q.push(mp(x, c2));
		}
	}
	return v;
}
void print(vector< vector<int> > &v){
	for(int i=0; i<size(v); i++){
		for(int j=0; j<size(v[i]); j++)
			cout<<v[i][j]<<" ";
		cout<<endl;
	}
	
}

int getMinimum(int c1, int c2, int r){
	vector<vector<int> > v = fillStates(c1, c2);
	//print(v);
	int m = INT_MAX;
	for(int i=r, j=0; j<=c2 && i<=c1; j++){
		m = min(m, v[i][j]);
	}
	for(int i=0, j=r; i<=c1 && j<=c2; i++){
		//cout<<v[i][j]<<endl;
		m = min(m, v[i][j]);
	}
	if(m == INT_MAX)
		m = -1;
	return m;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int c1, c2, r;
		cin>>c1>>c2>>r;
		int gc = gcd(c1, c2);
		if(r%gc != 0)
			cout<<-1<<endl;
		else
			cout<<getMinimum(c1, c2, r)<<endl;
	}
	return 0;
}