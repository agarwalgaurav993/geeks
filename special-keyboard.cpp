	#include <bits/stdc++.h>
	#define size(s) (int)s.size()
	#define pb push_back
	#define ll long long
	#define mp make_pair
	using namespace std;

	ll maxA(int n, vector<ll> &v){
		if(n > 75)
			return -1;
		if(v[n] != -1){
			return v[n];
		}

		v[n] = maxA(n-1, v) + 1;
		for(int i = n-3, k=2; i>=3; i--, k++)
			v[n] = max(maxA(i, v)*k, v[n]);

		//cout<<n<<"="<<v[n]<<endl;
		return v[n];

	}

	int main(){
		int T;
		cin>>T;
		vector<ll> v(76, -1);
		v[0] = 0;
		v[1] = 1;
		v[2] = 2;
		v[3] = 3;
		v[4] = 4;
		for(int f=1; f<=T; f++){
			int N;
			cin>>N;
			cout<<maxA(N, v)<<endl;
		}
		return 0;
	}