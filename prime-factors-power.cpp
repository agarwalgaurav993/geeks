#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
#define MAX 100001
using namespace std;

vector<int> seive(){
	vector<int> v(MAX);
	vector<int> p;
	for(int i=2; i<MAX; i++){
		if(v[i] != 0)
			continue;
		p.pb(i);
		for(int j = i+i; j<MAX ; j+= i){
			v[j] = 1;
		}
	}
	return p;
}

void powers(int N, vector<int> &p){

	for(int i=0; i<size(p) && N > 1; i++){
		int c = 0;
		while(N%p[i] == 0)
		{
			N = N/p[i];
			c++;
		}
		if(c)
			cout<<p[i]<<" "<<c<<" ";
	}
	cout<<endl;
}
int main(){
	int T;
	cin>>T;
	vector<int> p = seive();
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		powers(N, p);
	}
	return 0;
}