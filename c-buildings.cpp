#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define M 1000000007
#define mp make_pair
using namespace std;

vector<int> v(100001);
void ways(){
	v[0] = 1;
	v[1] = 2;
	for(int i = 2; i<=100000; i++){
		v[i] = (v[i-2] + v[i-1])%M;
	}
}


int main(){
	ways();
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;

		ll ans = v[N];
		ans = (ans*ans)%M;
		cout<<ans<<endl;
	}
	return 0;
}