#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int main(){
	int T;
	cin>>T;
	while(T--){
		int x, y, N;
		cin>>x>>y>>N;
		long long xtime=0, ytime = 0;
		int cnt = 0;
		if(y < x)
			swap(x, y);
		int yc = y;
		while(N != 0){
			int c = y/x;
			xtime += min(c, N)*x;
			N = N - min(c, N);
			if(N != 0){
				ytime += yc;
				N--;
				y = yc + y%x;	
			}
		}
		cout<<max(ytime, xtime)<<endl;
	}
	return 0;
}