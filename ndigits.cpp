#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

ll countNdigits(int n, int sum, vector<vector<int> > &v){
	if(sum < 0)
		return 0;
	if(n == 1){
		if(sum >0 && sum <= 9)
			return 1;
		else
			return 0;
	}
	if(v[n][sum] != -1)
		return v[n][sum];
	ll mc = 0;
	for(int i=0; i<=9; i++){
		if(countNdigits(n-1, sum -i, v) != -1)
			mc = mc + countNdigits(n-1, sum - i, v);
	}
	v[n][sum] = mc;
	return v[n][sum];
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, sum;
		cin>>N>>sum;
		vector<vector<int> > v(N+1, vector<int>(sum+1, -1));
		cout<<countNdigits(N, sum, v)<<endl;
	}
	return 0;
}