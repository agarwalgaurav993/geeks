#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
#define MAX 1000
using namespace std;
void seive(int start, int end, vector<int> &primes){
		vector<int> v(MAX, 1);
		for(int i = 0; i < size(primes); i++)
			for(int j = start/primes[i]; j <= end/primes[i]; j++)
				if( primes[i]*j >= start)
					v[ (primes[i]*j) - start ] = 0;
		for(int i = start; i < end; i++){
			if(v[ i - start ] && i > 1){
				primes.push_back(i);
				for(int p = i; p <= end/i; p++){
					v[ (i*p)- start ] = 0;
				}
			}
		}
	}

/*
Input: range - It should be in multiple of 10^5
Output : prime numbers
*/
vector<int> segmented_seive(int range){
	vector<int> primes;
	int s = 0, e = MAX;
	while(e <= range){
		seive(s, e, primes);
		//cout<<primes.size()<<endl;
		s = e;
		e += MAX;
	}
	return primes;
}
bool isGeek(int n, vector<int> &primes){
	for(int i=0; i<size(primes) && primes[i] <= floor(sqrt(n)); i++){
		if(n%primes[i] == 0)
			n = n/primes[i];
		if(n%primes[i] == 0)
			return false;
	}
	return true;
}
int main(){
	vector<int> primes = segmented_seive(MAX);
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int n;
		cin>>n;
		if(isGeek(n, primes))
			cout<<"Yes\n";
		else
			cout<<"No\n";
	}
	return 0;
}