#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;


int recur(int i, int j, vector<vector<int> > &v, vector<int> &f){
	if(i>j || i<0 || j<0 || i>=size(v) || j>=size(v))
		return 0;
	if(i == j)
		return f[i];
	//cout<<i<<" "<<j<<endl;
	if(v[i][j] != INT_MAX)
		return v[i][j];


	int freq = 0;
	for(int k=i; k<=j; k++)
		freq += f[k];
	for(int k=i; k<=j; k++){
		v[i][j] = min(v[i][j], freq + recur(i, k-1, v, f) + recur(k+1, j, v, f));
	}
	return v[i][j];
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t;
		cin>>N;
		vector<int> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		for(int i=0; i<N; i++){
			cin>>arr[i];
		}
		vector<vector<int> > v(N, vector<int>(N, INT_MAX));
		cout<<recur(0, N-1, v, arr)<<endl;
	}
	return 0;
}