#include <bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

struct node
{
	int data;
	struct node* next;
};

void splitList(struct node* source, struct node** frontRef, struct node** backRef)
{
    // Code here
    node *ptr1 = source;
    node *ptr2 = source;

    while(true){
    	if(!ptr2 || !ptr2->next || !ptr2->next->next)
    		break;
    	ptr2 = ptr2->next->next;
    	ptr1 = ptr1->next;
    }

    *frontRef = source;
    *backRef = ptr1->next;
    ptr1->next = NULL;
}


struct node* mergeList(struct node* a, struct node* b)
{
    // Code here
    
    if(!a)
    	return b;
    if(!b)
    	return a;
    node *res;
    if(a->data <= b->data){
    	res = a;
    	res->next = mergeList(a->next, b);
    }
    else{
    	res = b;
    	res->next = mergeList(a, b->next);
    }
    return res;
}
int main(){
	
	return 0;
}