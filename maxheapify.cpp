#include <bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

/*Assuming elements start from index 1*/

void max_heapify(vector<int> &v, int p){
	int N = v.size() - 1;
	int l = 2*p;
	int r = 2*p + 1;
	int m = p;
	if(l <= N && v[l] > v[m])
		m = l;
	if(r <= N && v[r] > v[m])
		m = r;
	if(m != p){
		swap(v[m], v[p]);
		max_heapify(v, m);
	}
}

void buildMaxHeap(vector<int> &v){
	int N = v.size() - 1;
	for(int p=N/2; p>=1; p--){
		max_heapify(v, p);
	}
}
void print(vector<int> &v){
	for(int i=1; i<v.size(); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}
int main(){
	int N, M;
	cin>>N>>M;
	vector<int> v(M+N+1);
	for(int i=1; i<M+N+1; i++)
		cin>>v[i];
	buildMaxHeap(v);
	print(v);
	return 0;
}