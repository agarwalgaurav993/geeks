#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int getAr(int i, int j,vector<vector<int> > & v){
	if(i>j || j<0 || i>=size(v))
		return 1;
	return v[i][j];
}
void print(vector<vector<int> > &v){
	for(int i=0; i<size(v); i++){
		for(int j=0; j<size(v[i]); j++)
			cout<<v[i][j]<<" ";
		cout<<endl;
	}
}
int countPalindromes(string &s){
	vector<vector<int> > v(s.length(), vector<int>(s.length()));
	for(int i=0; i<size(v); i++)
		v[i][i] = 1;
	for(int l=1; l < s.length(); l++){
		for(int i=0; i<s.length()-l; i++){
			int j = i + l;
			v[i][j] = s[i] == s[j] && getAr(i+1, j-1, v);
		}
	}
	int c = 0;
	for(int i=0; i<size(v); i++){
		for(int j=i+1; j<size(v[i]); j++){
			if(v[i][j])
				c++;
		}	
	}
	//print(v);
	return c;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		string s;
		cin>>s;
		cout<<countPalindromes(s)<<endl;
	}
	return 0;
}