#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

struct Trie{
	bool f;
	vector<Trie*> v;
	Trie(): f(false), v(vector<Trie*> (26)) {}
};

void Insert(Trie *root, string &s, int i){
	if(i == s.length()){
		root->f = true;
		return;
	}

	int j = s[i] - 'a';
	if(root->v[j] == NULL)
		root->v[j] = new Trie;
	return Insert(root->v[j], s, i+1);
}

bool search(Trie *root, string &q){
	string s;
	for(int i=0; i<q.length(); i++){
		if(!root || !root->v[q[i] - 'a'])
			return false;
		else
			root = root->v[q[i] - 'a'];
	}
	return root && root->f;
}
int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		Trie *root = new Trie;
		cin>>N;
		for(int i=0; i<N; i++){
			string s;
			cin>>s;
			Insert(root, s, 0);
		}

		string q;
		cin>>q;
		cout<<search(root, q)<<endl;
	}
	return 0;
}