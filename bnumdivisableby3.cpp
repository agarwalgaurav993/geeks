#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;


/*
Input: Binary String
*/
bool divisablebyN(string &bstring, int n){
	int r = 0;
	for(int i=0; i< bstring.length(); i++){
		if(bstring[i] == '0')
			r = (r<<1);
		else
			r = (r<<1) + 1;
		r = r%n;
	}
	return r == 0;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		string s;
		cin>>s;
		cout<<divisablebyN(s, 3)<<endl;
	}
	return 0;
}