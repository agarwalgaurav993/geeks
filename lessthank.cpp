#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

ll lessthank(vector<int> v, ll k){
	ll total = ((ll)size(v)*(size(v)+1))/2;
	ll p = 1;
	for(int i=0, s=0; i<= size(v);){

		if(p*v[i] < 0 || p*v[i] >= k){
			total -= size(v) - i;
			if(s < i)
				p = p/v[s];	
			else
				i++;
			s++;
		}
		else{
			p = p*v[i++];
		}
	}
	return total;
}

int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		ll K;
		cin>>N>>K;
		vector<int> v(N);
		for(int i=0; i<N; i++)
			cin>>v[i];
		cout<<lessthank(v, K)<<endl;
	}

	return 0;
}