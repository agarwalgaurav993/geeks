#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

void print(vector<vector<int> > &v){
	for(int i=0; i < size(v); i++){
		for(int j=0; j < size(v[i]); j++)
			cout<<v[i][j]<<" ";
		cout<<endl;
	}
}

map<int, string> m;
string recur(vector<vector<int> > &v, int i, int j){
	if(i == j){
		return m[i];
	}
	else return "(" + recur(v, i, v[i][j]) + recur(v, v[i][j]+1, j) + ")";
}
string matrixMul(vector<int> &v){

	vector<vector<int> > o(size(v)-1, vector<int>(size(v)-1));
	vector<vector<int> > t(size(v)-1, vector<int>(size(v)-1));

	for(int s=1; s<size(v)-1; s++){
		for(int i=0; i<size(v)-s-1; i++){
			int j = i+s;
			t[i][j] = INT_MAX;
			for(int k=i; k<j; k++){
				if(t[i][j] > t[i][k] + t[k+1][j] + v[i]*v[k+1]*v[j+1])
				{
					t[i][j] = min(t[i][j] , t[i][k] + t[k+1][j] + v[i]*v[k+1]*v[j+1]);
					o[i][j] = k;	
				}
				
			}
		}
	}
	//print(t);
	return recur(o, 0, size(o)-1);
	cout<<recur(o, 0, size(o)-1)<<endl;
}

int main(){
	string s = "A";
	for(int i=0; i<26; i++){
		m[i] = s;
		s[0] = s[0] + 1;
	}
		
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N, t;
		cin>>N;
		vector<int> arr;
		for(int i=0; i<N; i++){
			cin>>t;
			arr.push_back(t);
		}
		cout<<matrixMul(arr)<<endl;
	}
	return 0;
}