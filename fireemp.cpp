#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
#define MAX 100001
using namespace std;
vector<int> p(MAX, 1);

void seive(){
	p[0] = p[1] = 0;
	for(int i=2; i<MAX; i++){
		if(p[i] == 0)
			continue;
		for(int j=i+i;j<MAX; j+=i)
			p[j] = 0;
	}

}

int mysenior(vector<int> &v, vector<int> &s, int i){
	if(s[i] != -1)
		return s[i];
	s[i] = 1 + mysenior(v, s, v[i]);
	return s[i];
}
void print(vector<int> &v){
	for(int i=0; i<size(v); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}
int blacklisted(vector<int> &v){
	vector<int> nv(size(v), -1);
	for(int i=1; i<size(v); i++)
		if(v[i] == 0){
			nv[i] = 0;
			break;
		}

	for(int i=1; i<size(nv); i++){
		if(nv[i] == -1)
			mysenior(v, nv, i);
	}

	int c = 0;
	for(int i=1; i<size(nv); i++){
		if(nv[i] != 0 && p[nv[i] + i] == 1){
			c++;
		}
	}
	return c;
}

int main(){
	int T;
	cin>>T;
	seive();
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		vector<int> v(N+1);
		for(int i=1; i<=N; i++)
			cin>>v[i];
		cout<<blacklisted(v)<<endl;
	}
	return 0;
}