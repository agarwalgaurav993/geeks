#include <bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int main(){
	int T;
	cin>>T;
	while(T--){
		int a = 0;
		int b = 1;
		int N, d;
		cin>>N>>d;
		for(ll i=1; i<=N;){
			if(b%d == 0){
				cout<<b<<" ";
				i++;	
			}
			int c = a + b;
			a = b;
			b = c;
		}	
		cout<<endl;
	}
	return 0;
}