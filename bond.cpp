#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
#define MAX 1000003
using namespace std;
vector<ll> v(MAX, 1);

void mark_factors(){

	for(int i=2; i<MAX; i++){
		for(int j=i; j>=0 && j<MAX; j+=i){
			v[j]++;
		}
	}

	/*Sum*/
	for(int i=2; i<MAX; i++){
		v[i] = v[i-1] + v[i];
	}
}


int main(){
	mark_factors();
	v[0] = 0;
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int N;
		cin>>N;
		cout<<v[N]<<endl;
	}
	return 0;
}