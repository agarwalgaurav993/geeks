#include <bits/stdc++.h>
#define size(s) (int)s.size()
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;

int c = 0;

int gcd(int x, int y){
	if(y == 0)
		return c;
	c += x/y;
	return gcd(y, x%y);

}


int main(){
	int T;
	cin>>T;
	for(int f=1; f<=T; f++){
		int x, y;
		cin>>x>>y;
		c = 0;
		cout<<gcd(x, y)<<endl;
	}
	return 0;
}